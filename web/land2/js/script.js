(function ($) {
    $(document).ready(function () {
        $('#top-line').detach().insertBefore($('header.header .navbar .container'));

        // background video
        $("body > .body").tubular({videoId: "R6g2Ql4gpJ8", start: 10});

        // all girls
        $("#all-girls-show").on("click", function (e) {
            e.preventDefault();
            var girls = $("#all-girls");
            if (girls.hasClass("active")) {
                girls.slideUp().removeClass("active");
            } else {
                girls.slideDown().addClass("active");
            }
            $('html,body').animate({
                scrollTop: $("#girls").offset().top
            }, 1000);
        });
        $("#more-services-show").on("click", function (e) {
            e.preventDefault();
            var services = $("#more-services");
            if (services.hasClass("active")) {
                services.slideUp().removeClass("active");
            } else {
                services.slideDown().addClass("active");
            }
        });

        // scale
        var mw = $('#MobileOptimized').attr('content');
        if (mw <= 520) {
            var lw = 0;
            function setViewport() {
                var ww = $(window).width() < window.screen.width ? $(window).width() : window.screen.width;
                if (ww == lw) return;
                lw = ww;
                if (ww < mw) {
                    var ratio = ww / mw;
                    $('#viewport').attr('content', 'initial-scale=' + ratio + ', maximum-scale=' + ratio + ', minimum-scale=' + ratio + ', user-scalable=yes, width=' + ww);
                } else {
                    $('#viewport').attr('content', 'initial-scale=1.0, maximum-scale=2, minimum-scale=1.0, user-scalable=yes, width=' + ww);
                }
            }
            $(window).resize(setViewport);
            setViewport();
        }

        // sticky menu
        var sticky_menu_selector = 'header.header';
        $(sticky_menu_selector).clone().addClass('sticky').insertAfter(sticky_menu_selector);
        function stickIt() {
            var min_top = $(sticky_menu_selector).outerHeight();
            if ($(window).scrollTop() >= min_top) {
                $(sticky_menu_selector + '.sticky').fadeIn();
            } else {
                $(sticky_menu_selector + '.sticky').hide();
            }
        }
        setInterval(stickIt, 10);

        // scroll
        var scrollToAnchor = function (hash) {
            target = $(hash);
            if (!target.length) target = $('[name=' + hash.slice(1) + ']');
            if (!target.length) return false;
            $('html,body').animate({scrollTop: target.offset().top}, 1000);
            return false;
        };
        $('a[href*=#]:not([href=#])').on('click', function () {
            var curr_url = location.hostname + location.pathname.replace(/^\//, '');
            var link_url = this.hostname + this.pathname.replace(/^\//, '');
            if (curr_url == link_url) return scrollToAnchor(this.hash);
        });
        if (window.location.hash) {
            scrollToAnchor(window.location.hash);
        }

        // banner
        var banner_size = $('#banner .banner-size');
        function setHomeBannerSize() {
            var w = $('#banner').width();
            switch (true) {
                case w >= 1360:
                    banner_size.css('height', 680);
                    break;
                case w >= 680:
                    banner_size.css('height', w / 2);
                    break;
                case w >= 320:
                    banner_size.css('height', 340);
                    break;
                default:
                    banner_size.css('height', w / 0.941176471);
                    break;
            }
        }
        function setBannerSize() {
            var w = $('#banner').width();
            switch (true) {
                case w >= 1360:
                    banner_size.css('height', 340);
                    break;
                case w >= 680:
                    banner_size.css('height', w / 4);
                    break;
                case w >= 320:
                    banner_size.css('height', 170);
                    break;
                default:
                    banner_size.css('height', w / 1.882352941);
                    break;
            }
        }

        if ($('#banner.banner-home').length) {
            $(window).resize(function () {
                setHomeBannerSize()
            });
            setHomeBannerSize();
        } else {
            $(window).resize(function () {
                setBannerSize()
            });
            setBannerSize();
        }

        // girls images proportions
        function setGirlImageSize() {
            var w = 0;
            $.each($('.girl .girl-wrapper'), function (i, el) {
                w += $(el).parent().width();
            });
            $('.girl .girl-wrapper').css('height', w * 1.5 / 4).css('max-height', 'none');
        }
        $(window).resize(setGirlImageSize);
        setGirlImageSize();

        // slider
        $('.flex-viewport .slides li img').click(function () {
            $('.flex-direction-nav li a.flex-next').click();
        });
    });
})(jQuery);
