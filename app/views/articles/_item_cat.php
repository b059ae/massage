<?php
/** @var $item \yii\easyii\modules\article\api\ArticleObject */
/** @var $cat \yii\easyii\modules\article\api\CategoryObject */
use yii\easyii\modules\article\api\Article;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="blog-block col-md-6 col-sm-12 col-xs-12 text-left">
    <a href="<?= \yii\helpers\Url::to(['articles/cat', 'section'=>$cat->slug, 'category' => $item->slug]) ?>">
        <div class="text-center">
            <?= Html::img($item->thumb(null, 260)) ?>
        </div>
        <h3 class="text-uppercase"><?= $item->title ?></h3>
    </a>
    <?php /*
    foreach ($item->getItems() as $article) {
        echo $this->render('_item', ['item' => $article, 'cat' => $item, 'section' => $cat]);
    }*/
    ?>
</div>
