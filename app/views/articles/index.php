<?php
/** @var $cat \yii\easyii\modules\article\api\CategoryObject */

$this->registerMetaTag([
    'name' => 'description',
    'content' => $cat->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $cat->seo('keywords')
]);
$this->title = $cat->seo('title', $cat->title);
$this->params['breadcrumbs'][] = $cat->title;
?>

<section>
    <div>
        <div class="text-justify">
            <?= $cat->description; ?>
        </div>
        <div>
            <?php
            foreach ($cat->getChildren() as $child) {
                echo $this->render('_item_cat', ['item' => $child, 'cat' => $cat]);
            }
            ?>
        </div>
    </div>
</section>