<?php
/** @var $article \yii\easyii\modules\article\api\ArticleObject */
/** @var $cat \yii\easyii\modules\article\api\CategoryObject */
/** @var $section \yii\easyii\modules\article\api\CategoryObject|null */

use yii\easyii\modules\article\api\Article;
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $article->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $article->seo('keywords')
]);
$this->title = $article->seo('title', $article->title);
$sectionSlug = null;
if ($section) {
    $sectionSlug = $section->slug;
    $this->params['breadcrumbs'][] = ['label' => $section->title, 'url' => ['articles/index', 'section' => $sectionSlug]];
}
$this->params['breadcrumbs'][] = ['label' => $cat->title, 'url' => ['articles/cat', 'section'=>$sectionSlug, 'category' => $cat->slug]];
$this->params['breadcrumbs'][] = $article->title;
?>

<section>
    <div>
        <div class="text-justify">

        <?php if (count($article->photos)) : ?>
            <?= \yii\bootstrap\Carousel::widget([
                'options' => ['class' => 'slide'],
                /* 'clientOptions' => $this->clientOptions,*/
//                'controls' => false,
                'showIndicators' => true,
                'items' => array_map(function ($item) {
                    return [
                        'content' => Html::img($item->thumb(850, 350)),
                        'caption' =>$item->description
                    ];
                }, $article->photos),
            ]);
            ?>
        <?php endif; ?>
<br/>
        <div>
            <?= $article->getText() ?>
        </div>
<br/>

        <p>
            <?php foreach ($article->tags as $tag) : ?>
                <a href="<?= Url::to(['/articles/cat', 'slug' => $cat->slug, 'tag' => $tag]) ?>"
                   class="label label-info"><?= $tag ?></a>
            <?php endforeach; ?>
        </p>

        <p>
            <small class="text-muted"><strong>Дата
                    публикации:</strong> <?= \Yii::$app->formatter->asDate($article->time, 'long') ?></small>
        </p>
        <?php /*
        <p>
            <small class="text-muted"><strong>Просмотры:</strong> <?= $article->views ?></small>
        </p>
        */ ?>
    </div>
    </div>
</section>