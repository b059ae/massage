<?php
/** @var $item \yii\easyii\modules\article\api\ArticleObject */
/** @var $cat \yii\easyii\modules\article\api\CategoryObject */
/** @var $section \yii\easyii\modules\article\api\CategoryObject|null */
use yii\helpers\Html;
use yii\helpers\Url;
$sectionSlug = $section ? $section->slug : null;
$thumb = $item->thumb(null, 260);
?>
<div class="blog-block text-left">
    <a href="<?= \yii\helpers\Url::to(['articles/view', 'section'=>$sectionSlug, 'category'=>$cat->slug, 'slug' => $item->slug]) ?>">
        <?php if ($thumb): ?>
        <div class="text-center">
            <?= Html::img($thumb) ?>
        </div>
        <?php endif; ?>
        <h3 class="text-uppercase"><?= $item->title ?></h3>
        <p class="desc text-justify"><?= $item->short ?></p>
    </a>
        <?php /*<p class="date text-right"><?= \Yii::$app->formatter->asDate($item->time, 'long') ?></p>*/ ?>
</div>
