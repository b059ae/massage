<?php
/** @var $cat \yii\easyii\modules\article\api\CategoryObject */
/** @var $section \yii\easyii\modules\article\api\CategoryObject|null */

$this->registerMetaTag([
    'name' => 'description',
    'content' => $cat->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $cat->seo('keywords')
]);
$this->title = $cat->seo('title', $cat->title);
if ($section) {
    $this->params['breadcrumbs'][] = ['label' => $section->title, 'url' => ['articles/index', 'section' => $section->slug]];
}
$this->params['breadcrumbs'][] = $cat->title;
?>

<section>
    <div>
        <div class="text-justify">
            <?= $cat->description; ?>
        </div>
        <div>
            <?php
            foreach ($cat->getItems(['pagination' => ['pageSize' => 6]]) as $article) {
                echo $this->render('_item', ['item' => $article, 'cat' => $cat, 'section' => $section]);
            }
            ?>
        </div>
    </div>
</section>