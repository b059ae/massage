<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 18.08.2017
 * Time: 23:07
 */
/** @var $modalId string */
?>

<div id="<?=$modalId?>" class="modal-window-wrapper" style="display: none;">
    <div class="modal-window-container">
        <div id="jivo-close-button" class="modal-window-close">
            <svg id="jivo-icon-closewidget" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <circle class="jivo-st0" cx="12" cy="12" r="11"></circle>
                <path class="jivo-st1" d="M7.5 16.5l9-9M16.5 16.5l-9-9"></path>
            </svg>
        </div>
        <div class="modal-window-header">
            <h4 class="modal-window-title">Оставьте свой отзыв</h4>
        </div>
        <div class="overlay-window">
            <?= \app\widgets\GuestbookForm::widget([
                'id'=>'form-guestbook',
            ]) ?>
        </div>
    </div>
</div>
