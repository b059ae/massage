<?php
/**
 * @var $page \yii\easyii\modules\page\api\PageObject
 * @var $guestbook \yii\easyii\modules\guestbook\api\PostObject[]
 * @var $salony yii\easyii\modules\entity\api\ItemObject[]
 */
$asset = \app\assets\AppAsset::register($this);

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->seo('keywords')
]);
$this->title = $page->seo('title');
?>
<div id="guestbook" class="feedback-home text-center pattern">
    <div class="feedback-fill">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 feedback-size">
                    <h2>Отзывы</h2>
                    <p><a class="btn btn-info btn-lg modal-window-button" href="#form-guestbook-modal">Оставить
                            отзыв</a></p>
                    <?php foreach ($guestbook as $item): ?>
                        <span class="quote">“</span>
                        <span class="meta">Пишет <?= $item->getName() ?>, <?= Yii::$app->formatter->asDatetime($item->time, 'short') ?></span>
                        <span class="comment"><?= $item->getText() ?></span>
                        <?php if ($item->getAnswer()) :?>
                            <span class="meta">Ответ Администратора</span>
                            <span class="comment"><?= $item->getAnswer() ?></span>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end #guestbook -->
<?= $this->render('//guestbook/_modal', ['modalId' => 'form-guestbook-modal']); ?>

<?= $this->render('//site/slides/map', ['asset' => $asset, 'salony'=>$salony]); ?>