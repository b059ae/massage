<?php
/**
 * @var $page \yii\easyii\modules\page\api\PageObject
 * @var $girl \yii\easyii\modules\catalog\api\ItemObject
 * @var $girls \yii\easyii\modules\catalog\api\ItemObject[]
 * @var $salony yii\easyii\modules\entity\api\ItemObject[]
 * @var $guestbook \yii\easyii\modules\guestbook\api\PostObject[]
 */
$asset = \app\assets\AppAsset::register($this);

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->seo('keywords')
]);
$this->title = $page->seo('title') . ' - ' . $girl->getTitle();
?>
<!-- Профайл -->
<?= $this->render('_view', ['girl' => $girl]); ?>
<!-- /Профайл -->
<?= $this->render('//site/slides/girls', ['asset' => $asset, 'girls' => $girls, 'title' => 'Другие мастера']); ?>
<?= $this->render('//site/slides/guestbook', ['asset' => $asset, 'guestbook' => $guestbook]); ?>
<?= $this->render('//site/slides/map', ['asset' => $asset, 'salony' => $salony]); ?>
