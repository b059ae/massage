<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 22.08.17
 * Time: 18:01
 */
/** @var $girl \yii\easyii\modules\catalog\api\ItemObject  */
?>
<div class="col-sm-6 col-md-3 girl">
    <a href="<?= \yii\helpers\Url::to(['/girls/view', 'slug' => $girl->slug]) ?>">
        <div class="girl-wrapper"
             style="background-image: url('<?= $girl->thumb(267, 400) ?>')">
            <!--<a href="#" class="girl_link">-->
            <div class="info">
                <h4 class="name"><?= $girl->title ?></h4>
            </div>
        </div>
    </a>
</div>

