<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 22.08.17
 * Time: 18:14
 */

use yii\easyii\modules\catalog\api\PhotoObject;
use yii\helpers\Html;

/** @var $girl \yii\easyii\modules\catalog\api\ItemObject */
\yii\easyii\widgets\Fancybox::widget([
    'selector' => '.fancybox',
]);
?>
<div id="girl" class="black text-center">
    <h2><?= $girl->title ?></h2>
    <div class="map-fill">
        <div class="container">
            <div class="row single_girl">
                <div class="col-md-5">
                    <!-- Основная картинка -->
                    <?= Html::img($girl->thumb(740)); ?>
                    <!-- /Основная картинка -->
                </div>
                <div class="col-md-7">
                    <!-- Форма для записи -->
                    <h4 class="name">Записаться на прием</h4>
                    <div id="appointment-form">
                        <?= \app\widgets\GalleryForm::widget([
                            'id' => 'form-gallery',
                        ]) ?>
                    </div>
                    <p>Перезвоним за 27 секунд</p>
                    <p>Ваши контактные данные не будут
                        передаваться третьим лицам</p>
                    <!-- /Форма для записи -->
                    <div class="row images">
                        <!-- Дополнительные картинки -->
                        <div class="image col-md-3 col-xs-6">
                            <a href="<?= $girl->getImage() ?>" class="fancybox">
                                <?= Html::img($girl->thumb(120, 160)); ?>
                            </a>
                        </div>
                        <?php
                        /** @var PhotoObject $photo */
                        foreach (array_slice($girl->getPhotos(), 0, 3) as $photo) : ?>
                            <div class="image col-md-3 col-xs-6">
                                <a href="<?= $photo->getImage() ?>" class="fancybox">
                                    <?= Html::img($photo->thumb(120, 160)); ?>
                                </a>
                            </div>
                        <?php endforeach; ?>
                        <!-- /Дополнительные картинки -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- end #map -->

