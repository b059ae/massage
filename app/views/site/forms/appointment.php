<?php
/**
 * @var $signup \app\models\AppointmentForm
 */
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>

<div style="display: none;">
    <div id="form-appointment" class="overlay-window">
        <?php $form = ActiveForm::begin([
            'id' => 'form-reg',
            'enableClientValidation' => true,
            'action' => Url::to(['/site/signup'])
        ]); ?>
            <?= $form->field($signup, 'name', [
                'template' => "{input}\n{error}",
                'inputOptions' => [
                    'class' => 'form-control input-lg',
                ],
            ])
            ->textInput([
                'placeholder' => $signup->getAttributeLabel('name')
            ]); ?>

            <?= $form->field($signup, 'phone', [
                'template' => "{input}\n{error}",
                'inputOptions' => [
                    'class' => 'form-control input-lg',
                ],
            ])
            ->textInput([
                'placeholder' => $signup->getAttributeLabel('phone')
            ]); ?>

            <?php /*

            <?= $form->field($signup, 'date', [
                'template' => "{input}\n{error}",
                'inputOptions' => [
                    'class' => 'form-control input-lg',
                ],
            ])
            ->textInput([
                'placeholder' => $signup->getAttributeLabel('date')
            ]); ?>

            <?= $form->field($signup, 'time', [
                'template' => "{input}\n{error}",
                'inputOptions' => [
                    'class' => 'form-control input-lg',
                ],
            ])
            ->textInput([
                'placeholder' => $signup->getAttributeLabel('time')
            ]); ?>

            */ ?>

            <?= $form->field($signup, 'masseuse', [
                'template' => "{input}\n{error}",
                'inputOptions' => [
                    'class' => 'form-control input-lg',
                ],
            ])
            ->dropDownList($signup->masseuseList(), [
                'prompt' => $signup->getAttributeLabel('masseuse'),
            ]); ?>

            <div class="btn-send">
                <input type="submit" id="btn-send" value="Записаться" class="btn btn-info btn-lg">
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
