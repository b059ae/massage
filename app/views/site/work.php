<?php
/**
 * @var $page \yii\easyii\modules\page\api\PageObject
 * @var $interery \yii\easyii\modules\gallery\api\PhotoObject[]
 * @var $salony yii\easyii\modules\entity\api\ItemObject[]
 */
$asset = \app\assets\AppAsset::register($this);

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->seo('keywords')
]);
$this->title = $page->seo('title');
?>

<?= $this->render('slides/place', ['asset' => $asset, 'interery' => $interery]); ?>
<?= $this->render('slides/money', ['asset' => $asset]); ?>
<?= $this->render('slides/choise', ['asset' => $asset]); ?>
<?= $this->render('slides/zone', ['asset' => $asset, 'interery' => $interery]); ?>
<?= $this->render('slides/map', ['asset' => $asset, 'salony' => $salony]); ?>
