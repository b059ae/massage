<?php
/** @var $asset yii\web\AssetBundle */

/** @var $salony yii\easyii\modules\entity\api\ItemObject[] */

use yii\easyii\models\Setting;

?>
<div id="map" class="black map-home text-center">
    <div class="map-fill">
        <h2>Карта проезда</h2>
        <div class="container">
            <div class="row">
                <?php foreach ($salony as $item): ?>
                    <div class="col-md-4">
                        <div class="contact">
                            <a class="map icon fa-location-arrow"
                               href="https://yandex.ru/maps/?um=constructor%3A<?= $item->yandex ?>"
                               target="_blank"><?= $item->addresstext ?></a>
                            <a class="map icon fa-phone" target="_blank"
                               href="tel:+7<?= \app\helpers\Html::removeNonNumeric($item->phone) ?>">8 <?= $item->phone ?></a>
                        </div>
                        <?php /*<span class="phone icon fa-phone" id="phone-contact-01"><?=$item->phone?></span> */ ?>
                        <div id="map-one" class="white">
                            <div class="map-wrapper">
                                <?php
                                if (strlen($item->yandex) > 0): ?>
                                    <script type="text/javascript" charset="utf-8" async
                                            src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A<?= $item->yandex ?>&amp;width=100%25&amp;height=100%&amp;lang=ru_RU&amp;scroll=false"></script>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div> <!-- end #map -->
