<?php
/** @var $programmy \yii\easyii\modules\catalog\api\ItemObject[] */
?>
<div id="services" class="white pattern white-home">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Это стоит попробовать</h2>
                <div class="services services-home default">
                    <?php foreach (array_slice($programmy, 0, 8) as $item): ?>
                        <div class="service">
                            <div class="meta">
                                <span class="video hidden-xs"><a class="btn btn-info btn-sm" href="#">Просмотр видео</a></span>
                                <span class="duration">1 час</span>
                                <span class="price"><?=$item->getPrice()?>&nbsp;р.</span>
                            </div>
                            <div class="name"><a href="#"><?=$item->title?></a></div>
                            <div class="content">
                                <?=\app\helpers\Html::stopIframe($item->description) ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div id="more-services" class="services services-home default" style="display: none;">
                    <?php foreach (array_slice($programmy, 8) as $item): ?>
                        <div class="service">
                            <div class="meta">
                                <span class="video hidden-xs"><a class="btn btn-info btn-sm" href="#">Просмотр видео</a></span>
                                <span class="duration">1 час</span>
                                <span class="price"><?=$item->getPrice()?>&nbsp;р.</span>
                            </div>
                            <div class="name"><a href="#"><?=$item->title?></a></div>
                            <div class="content">
                                <?=\app\helpers\Html::stopIframe($item->description) ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="text-center">
                    <!--<a id="more-services-show" class="btn btn-info btn-lg btn-services" href="#">Смотреть все программы</a>-->
                </div>
            </div> <!-- end #services -->

        </div>
    </div>
</div> <!-- end #services -->
<?php
$script = <<< JS
        //Подробное описание программ
        (function($){
            $('.services .service .name a, .services .service .video').on('click', function(e){
                e.preventDefault();
                toggleCaret($(this));
            });
            function toggleCaret (a) {
                 var service = a.closest('.service');
                 service.find('.content').slideToggle('fast', function(){
                     if($(this).is(':visible')){
                         $(this).find("iframe").prop("src", function(){
                            // Set their src attribute to the value of data-src
                            return $(this).data("src");
                        });
                     }
                 });
            }
        })(jQuery);
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>

