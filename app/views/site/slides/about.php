<?php
/** @var $asset yii\web\AssetBundle */
/** @var $programmy \yii\easyii\modules\catalog\api\ItemObject[] */
?>

<div id="about" class="white about-home white-home pattern text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Почему «Каприз»?</h2>
                <div class="row options text-left">
                    <div class="col-md-4 col-sm-6">
                        <h4>Все массажистки выполняют массаж обнажёнными</h4>
                        <p class="icon"><img src="/images/ico12.png"
                                             class="attachment-post-thumbnail wp-post-image" alt="Massage_1_ico_12"
                                             width="70" height="70"></p>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h4>Устраняем случайные встречи</h4>
                        <p class="icon"><img src="/images/ico07.png"
                                             class="attachment-post-thumbnail wp-post-image" alt="Massage_1_ico_7"
                                             width="70" height="70">Отдельные зоны для встречи гостей. Строгий контроль выхода гостей из комнат.</p>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h4>Комфортабельные массажные комнаты</h4>
                        <p class="icon"><img src="/images/ico08.png"
                                             class="attachment-post-thumbnail wp-post-image" alt="Massage_1_ico_8"
                                             width="70" height="70">С джакузи. Всегда чисто и уютно.</p>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h4>Организация закрытых мероприятий</h4>
                        <p class="icon"><img src="/images/ico01.png"
                                             class="attachment-post-thumbnail wp-post-image" alt="Massage_1_ico_1"
                                             width="70" height="70">Мальчишник, сюрпризы, подарочные сертификаты.</p>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h4>Конфиденциальность и безопасность</h4>
                        <p class="icon"><img src="/images/ico12.png"
                                             class="attachment-post-thumbnail wp-post-image" alt="Massage_1_ico_12"
                                             width="70" height="70">Зонт, который закрывает лицо при входе в помещение.</p>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h4>Одноразовые принадлежности</h4>
                        <p class="icon"><img src="/images/ico04.png"
                                             class="attachment-post-thumbnail wp-post-image" alt="Massage_1_ico_4"
                                             width="70" height="70">После каждого гостя проводится санобработка.</p>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h4>Семейные программы</h4>
                        <p class="icon"><img src="/images/ico12.png"
                                             class="attachment-post-thumbnail wp-post-image" alt="Massage_1_ico_1"
                                             width="70" height="70">На Ваш выбор две девушки или мастер мужчина + девушка.</p>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h4>Закрытая охраняемая парковка</h4>
                        <p class="icon"><img src="/images/ico02.png"
                                             class="attachment-post-thumbnail wp-post-image" alt="Massage_1_ico_2"
                                             width="70" height="70">Наклейка на номерной знак во время нахождения машины на парковке.</p>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h4>Все виды массажа</h4>
                        <p class="icon"><img src="/images/ico12.png"
                                             class="attachment-post-thumbnail wp-post-image" alt="Massage_1_ico_1"
                                             width="70" height="70">Классический, восточный, тайский, боди, лингам.</p>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h4>Гиппоалергенное массажное масло</h4>
                        <p class="icon"><img src="/images/ico04.png"
                                             class="attachment-post-thumbnail wp-post-image" alt="Massage_1_ico_4"
                                             width="70" height="70">Без запаха.</p>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h4>От 5 девушек в каждой смене.</h4>
                        <p class="icon"><img src="/images/ico03.png"
                                             class="attachment-post-thumbnail wp-post-image" alt="Massage_1_ico_3"
                                             width="70" height="70">Подберёт массажную программу, учитывая все Ваши тайные желания </p>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h4>Бар, Кальян</h4>
                        <p class="icon"><img src="/images/ico01.png"
                                             class="attachment-post-thumbnail wp-post-image" alt="Massage_1_ico_10"
                                             width="70" height="70">Русский бильярд, WI-FI зона для всех гостей</p>
                    </div>
                </div>
                <div class="about_div">
                    <h2>«Каприз» — сеть салонов эротического массажа<br/>в Ростове-на-Дону</h2>
                </div>
            </div>
        </div>
    </div>
</div> <!-- end #about -->
