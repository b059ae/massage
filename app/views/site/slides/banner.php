<?php
/** @var $asset yii\web\AssetBundle */
/** @var $page \yii\easyii\modules\page\api\PageObject */
/** @var $salony yii\easyii\modules\entity\api\ItemObject[] */

use app\models\AppointmentForm;
use app\models\Header;

$appointment = new AppointmentForm();
?>

<div id="banner" class="banner-home">
    <div class="banner-fill">
        <div class="container-fluid">
            <div class="row header_top_banner">
                <div class="col-md-12 banner-size fullscreen-bg">
                    <div class="video-button hidden-xs hidden-sm">
                        <a class="overlay-video" href="https://www.youtube.com/embed/2GiEqyIS2-I?rel=0&showinfo=0&autoplay=1" onclick="metrikaReachGoal('video'); return true;">
                            <img src="/images/play-button.png">
                        </a>
                    </div>
                    <div class="overlay">
                        <div class="flags"></div>
                        <?php /* <div class="contacts">
                            foreach ($salony as $item):?>
                                <div class="contact">
                                    <span id="phone01"><a class="phone full" href="tel:+<?=Html::removeNonNumeric($item->phone)?>"><?=$item->phone?></a></span>
                                    <span class="metro full"><?=$item->addresstext?></span>
                                    <span class="time full icon fa-clock-o">круглосуточно</span>
                                    <span class="map full icon fa-location-arrow"><a href="#map">Схема проезда</a></span>
                                    <span class="map mobile icon fa-location-arrow"><?=$item->addresshort?> <?=$item->phone?></span>
                                </div>
                            <?php endforeach;
                        </div> */ ?>
                        <div class="banner-head">
                            <h1><?= $page->seo('h1'); ?></h1>
                            <?php /*<h1>Мы работаем, чтобы Вы расслабились!</h1>*/ ?>
                        </div>
                        <div class="appointment-head">
                            <div class="container appointment-vertical">
                                <div class="row">
                                    <?php /* <div class="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-1 col-sm-5 col-sm-offset-1 hidden-xs">
                                        <div id="video-autosize" class="video-frame">
                                            <iframe src="https://www.youtube.com/embed/9iHM6X6uUH8" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div> */ ?>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <h3 class="stock-head marked">Акция!</h3>
                                        <p><?= Header::getSlogan(); ?></p>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center">
                                    <?php if (($timeout = Header::getEndTime()) > 0) : ?>
                                        <p>До конца акции осталось:</p>
                                        <div id="stock-timeout" data-timeout="<?= $timeout; ?>">
                                            <span class="stock-timer">
                                                <span class="stock-line"></span>
                                                <span class="stock-number" id="stock-timeout-h"><?= date('H', $timeout); ?></span>
                                            </span>
                                            <span class="stock-breaker">:</span>
                                            <span class="stock-timer">
                                                <span class="stock-line"></span>
                                                <span class="stock-number" id="stock-timeout-m"><?= date('i', $timeout); ?></span>
                                            </span>
                                            <span class="stock-breaker">:</span>
                                            <span class="stock-timer">
                                                <span class="stock-line"></span>
                                                <span class="stock-number" id="stock-timeout-s"><?= date('s', $timeout); ?></span>
                                            </span>
                                        </div>
                                    <?php endif; ?>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
                                        <div class="appointment-wrapper">
                                            <p>Мы перезвоним за 27 секунд!</p>
                                            <p><a class="btn btn-info btn-lg modal-window-button" href="#form-callback-modal">Оставить заявку</a></p>
                                            <p><small style="font-size: 70%;">Ваши контактные данные не будут<br>передаваться третьим лицам</small></p>
                                            <?php /*
                                            <div class="appointment-form">
                                                <?= \app\widgets\CallbackForm::widget([
                                                    'id'=>'form-appointment',
                                                    'button' => 'Получить скидку!',
                                                    'layout' => 'inline',
                                                ]) ?>
                                                $form = ActiveForm::begin([
                                                'id' => 'form-appointment',
                                                'layout' => 'inline',
                                                'enableClientValidation' => true,
                                                'action' => Url::to(['/site/signup'])
                                            ]); ?>
                                                <?= $form->field($appointment, 'phone', [
                                                    'template' => "{input}\n{error}",
                                                    'labelOptions' => [
                                                        'class' => 'sr-only',
                                                    ],
                                                ])->widget(\yii\widgets\MaskedInput::className(), [
                                                    'mask' => '+7(999) 999-99-99',
                                                    'options' => [
                                                        'autocomplete' => 'off',
                                                        'class' => 'appointment-phone-input form-control',
                                                        'placeholder' => $appointment->getAttributeLabel('phone')
                                                    ],
                                                ]); ?>
                                                <div class="form-group">
                                                    <input type="submit" id="btn-send" value="Записаться" class="btn btn-info">
                                                </div>
                                            <?php ActiveForm::end();
                                            </div> */ ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php /*<div class="head-contacts hidden-sm hidden-xs">
                            <h4 class="text-center">Наши салоны:</h4>
                            <ul class="list-inline">
                            <?php foreach ($salony as $item): ?>
                                <li class="place-contact">
                                    <a href="#map"><span class="map full icon fa-location-arrow"></span> <?= $item->addresstext; ?></a><br>
                                    <a class="btn btn-info" href="#map">Позвонить</a>
                                </li>
                            <?php endforeach; ?>
                            </ul>
                        </div>*/ ?>
                    </div>
                    <?php /*
                    <video muted autoplay poster="<?= $asset->baseUrl ?>/images/banner.jpg" id="video_background">
                        <source src="<?= $asset->baseUrl ?>/video/banner.mp4" type="video/mp4">
                        <source src="<?= $asset->baseUrl ?>/video/banner.webm" type="video/webm">
                    </video>
                    <div class="video-overlay"></div>*/ ?>
                </div>
            </div>
        </div>
    </div>
</div> <!-- end #banner -->

<div id="video-hidden-slide" class="container-fluid pattern">
    <div class="row">
        <div class="hidden-lg hidden-md col-sm-12 col-xs-12">
            <div class="video-frame">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/2GiEqyIS2-I?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>

<?php /*
<div class="fullscreen-bg">
    <div class="overlay banner-head">
        <div class="flags"></div>
        <div class="contacts">
            <?php foreach ($salony as $item):?>
                <div class="contact">
                    <span id="phone01"><a class="phone full" href="tel:+<?=Html::removeNonNumeric($item->phone)?>"><?=$item->phone?></a></span>
                    <span class="metro full"><?=$item->addresstext?></span>
                    <span class="time full icon fa-clock-o">круглосуточно</span>
                    <span class="map full icon fa-location-arrow"><a href="#map">Схема проезда</a></span>
                    <span class="map mobile icon fa-location-arrow"><?=$item->addresshort?> <?=$item->phone?></span>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="banner-head">
            <h1><?=$page->seo('h1')?></h1>
            <h2 class="hidden-sm hidden-xs">Сексуальные девушки, владеющие всеми техниками СПА и эротического релакса, уже ждут вас - выбирайте на сайте или прямо в клубе!</h2>
            <!--<a class="btn btn-info btn-lg" href="#">Записаться</a>-->
        </div>
    </div>
    <video muted autoplay poster="video/plane.jpg" class="fullscreen-bg__video">
        <source src="<?= $asset->baseUrl ?>/video/banner.mp4" type="video/mp4">
        <source src="<?= $asset->baseUrl ?>/video/banner.webm" type="video/webm">
    </video>
</div>
*/ ?>

<?php /*
$script = <<< JS
    var video = $('#video_background');
    var image = $('#image_background');
    
    video.on('ended', function() {
        $(this).fadeOut('200',function(){//$(this) refers to video
            //image.fadeIn('200');
        });
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
 */
?>