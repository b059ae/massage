<?php
/** @var $akcii \yii\easyii\modules\carousel\api\CarouselObject[] */
?>
<div id="actions" class="black actions-home text-center">
    <div class="actions-fill">
        <h2>Выгодные предложения</h2>
        <div class="container">
            <div class="row">
                <div class="col-md-12 actions-size hidden-xs">
                    <div style="max-width: 750px; margin: 0 auto;"
                         class="metaslider metaslider-flex metaslider-actions-home ml-slider">
                        <div id="metaslider_container_actions_home">
                            <div id="metaslider_actions_home" class="flexslider" style="height: 500px;">
                                <ul class="slides" style="width: 1000%; transition-duration: 0s; transform: translate3d(-750px, 0px, 0px);">
                                    <?php foreach ($akcii as $akciya): ?>
                                        <li style="display: block; width: 750px; float: left;" class="ms-image clone" aria-hidden="true">
                                            <img src="<?=$akciya->thumb(750, 500)?>" alt="" draggable="false" class="img-responsive">
                                            <div class="caption-wrap">
                                                <div class="caption">
                                                    <h3><?=$akciya->title?></h3>
                                                    <p><?=$akciya->text?></p>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 actions-size visible-xs">
                    <?php foreach ($akcii as $akciya): ?>
                        <div class="caption-xs">
                            <h3><?=$akciya->title?></h3>
                            <p><?=$akciya->text?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div> <!-- end #actions -->
