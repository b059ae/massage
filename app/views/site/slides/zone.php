<?php
/**
 * @var $asset yii\web\AssetBundle
 * @var $interery \yii\easyii\modules\gallery\api\PhotoObject[]
 */
?>

<div id="zone" class="pattern">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Комфортная зона отдыха для персонала</h2>
                <div class="row more-size">
                    <div class="col-md-6 col-xs-12">
                        <div class="row">
                        <div style="max-width: 520px; margin: 0 auto;"
                             class="metaslider metaslider-flex metaslider-actions-home ml-slider">
                            <div id="metaslider_container_action_home">
                                <div id="metaslider_actions_home" class="flexslider" style="height: 345px;">
                                    <ul class="slides" style="width: 1000%; transition-duration: 0s; transform: translate3d(-750px, 0px, 0px);">
                                        <?php foreach ($interery as $item): ?>
                                            <li style="display: block; width: 520px; float: left;" class="ms-image clone" aria-hidden="true">
                                                <a class="overlay-image" href="<?= $item->thumb(675, 445); ?>">
                                                    <img src="<?=$item->thumb(520, 345)?>" alt="" draggable="false" class="img-responsive">
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <h4>Ваш комфорт:</h4>
                        <p>В вашем распоряжении комфортные зоны для персонала, в которых вы можете отдохнуть, сделать макияж и приготовиться к работе. Если вам негде жить, в первое время вы можете жить и работать в салоне</p>
                        <h4>Безопасность:</h4>
                        <p>В сети салонов каждый день работает охранник, который придет к вам в комнату по нажатию секретной кнопки. Гости об этом знают, поэтому таких ситуаций у нас не бывает</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
