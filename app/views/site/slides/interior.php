<?php
/** @var $interery \yii\easyii\modules\gallery\api\PhotoObject[] */
?>
<div id="interior" class="interior-home pattern">
    <h2>Интерьеры сети салонов</h2>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 images">
                <div class="row">
                    <div style="max-width: 520px; margin: 0 auto;"
                         class="metaslider metaslider-flex metaslider-actions-home ml-slider">
                        <div id="metaslider_container_interior_home">
                            <div id="metaslider_interior_home" class="flexslider" style="height: 345px;">
                                <ul class="slides" style="width: 1000%; transition-duration: 0s; transform: translate3d(-750px, 0px, 0px);">
                                    <?php foreach ($interery as $item): ?>
                                        <li style="display: block; width: 520px; float: left;" class="ms-image clone" aria-hidden="true">
                                            <a class="overlay-image" href="<?= $item->thumb(675, 445); ?>">
                                                <img src="<?=$item->thumb(520, 345)?>" alt="" draggable="false" class="img-responsive">
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php /* foreach ($interery as $item): ?>
                        <div class="col-sm-6 interior">
                            <!--<a href="#">-->
                            <img src="<?=$item->thumb(263, 175)?>">
                            <!--</a>-->
                        </div>
                    <?php endforeach;*/ ?>
                </div>
            </div>
            <div class="col-sm-6 info">
                <div class="text">
                    <p>Кокетливая игра теней на стенах и легкая музыка помогают расслабиться, а также
                        ощутить всю прелесть жизни. Шикарные интерьеры сети эротических салонов «Каприз» располагают к
                        приятному отдыху с очаровательной девушкой. Забота о каждом клиенте сразу заметна еще при первом
                        взгляде на изысканные апартаменты. Вас ждет чистое одноразовое белье и полотенца, а также
                        современный душ, в котором так приятно оказаться вдвоем с незнакомкой. Здесь все создано и
                        предусмотрено для Вашего комфорта.</p>
                    <p>Уютная и расслабляющая атмосфера помогает настроиться на нужный лад. Интерьеры сети эротических
                        салонов поражают своей красотой и роскошью. Приятная цветовая гамма наблюдается во всем.
                        Стены и шторы королевских расцветок не отвлекают, а наоборот помогают получать удовольствие
                        от полноценного отдыха. Особое очарование дарят приглушенный свет и зажженные свечи. При
                        желании всегда можно побаловать себя оригинальным коктейлем или чашечкой кофе, который будет
                        приготовлен исключительно для Вас.</p>
                </div>
            </div>
        </div>
    </div>
</div> <!-- end #interior -->
