<?php
/** @var $asset yii\web\AssetBundle */
?>

<div id="money" class="black">
    <div class="money-fill">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Сколько вы сможете заработать?</h2>
                    <div class="row more-size">
                        <div class="col-md-8 col-xs-12 money-info">
                            <p>Время проведенное с вами гостем. Вам оплачивается каждая минута - 40% с каждой
                                программы.</p>
                            <p>В день у вас будет от 2 до 10 программ: 2400 * 5 программ = 12 000 рублей. Чаевые все
                                Вам!</p>
                            <p>При графике 2/2 за месяц вы можете заработать от 90 000 до 310 000 рублей- и это строго
                                без интима!</p>
                            <p>В первый месяц можно гарантированно заработать от 40 000 рублей!</p>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <p class="text-center"><img class="round-image"
                                                        src="/images/image_ashx.jpg"/></p>
                            <div class="money-manager">
                                <p class="text-center"><strong>Мария Назарова</strong></p>
                                <p class="text-center">Управляющая сети салонов<br>+7 (863) 270-99-35</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
