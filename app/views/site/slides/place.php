<?php
/**
 * @var $asset yii\web\AssetBundle
 * @var $interery \yii\easyii\modules\gallery\api\PhotoObject[]
 */
?>

<div id="place" class="place-home pattern">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Работа массажисткой в сети салонов &laquo;Каприз&raquo;</h1>
                <h2>Строго без интима</h2>
                <div class="row">
                    <div class="col-md-6 info">
                        <div class="text">
                            <p class="text-justify">Сеть комплексов эротического массажа &laquo;Каприз&raquo; работает с 2012 года. За это время нам удалось
                                <strong>сохранить свою репутацию</strong> приличного заведения <strong>без предоставления услуг
                                интимного характера</strong>. На данный момент у нас работает более 30 массажисток со всей РФ.</p>
                        </div>
                    </div>
                    <div class="col-md-6 images">
                        <div style="max-width: 520px; margin: 0 auto;"
                             class="metaslider metaslider-flex metaslider-actions-home ml-slider">
                            <div id="metaslider_container_interior_home">
                                <div id="metaslider_interior_home" class="flexslider" style="height: 345px;">
                                    <ul class="slides" style="width: 1000%; transition-duration: 0s; transform: translate3d(-750px, 0px, 0px);">
                                        <?php foreach ($interery as $item): ?>
                                            <li style="display: block; width: 520px; float: left;" class="ms-image clone" aria-hidden="true">
                                                <a class="overlay-image" href="<?= $item->thumb(675, 445); ?>">
                                                    <img src="<?=$item->thumb(520, 345)?>" alt="" draggable="false" class="img-responsive">
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
