<?php
/** @var $girls \yii\easyii\modules\catalog\api\ItemObject[] */
/** @var $title string */

$countInRow = 4;
?>
<div id="girls" class="girls-home pattern text-center">
    <?php /*<div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3 girl-video">
                <iframe src="https://player.vimeo.com/video/214216580" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
    </div>*/ ?>
    <h2><?= $title ?></h2>
    <div id="top-girls" class="container">
        <div class="row">
            <?php foreach (array_slice($girls, 0, $countInRow) as $girl): ?>
                <?= $this->render('//girls/_item', ['girl' => $girl]); ?>
            <?php endforeach; ?>
        </div>
    </div>

    <?php if (count($girls) > $countInRow) : ?>
        <div id="all-girls" class="container" style="display: none;">
            <div class="row">
                <?php foreach (array_slice($girls, $countInRow) as $girl): ?>
                    <?= $this->render('//girls/_item', ['girl' => $girl]); ?>
                <?php endforeach; ?>
            </div>
        </div>

        <button id="all-girls-show" class="btn btn-info btn-lg">Все девушки</button>
    <?php endif; ?>
</div><!-- end #girls -->
