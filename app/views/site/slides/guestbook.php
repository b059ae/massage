<?php
/** @var $guestbook \yii\easyii\modules\guestbook\api\PostObject[] */
?>
<div id="guestbook" class="black feedback-home text-center">
    <div class="feedback-fill">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 feedback-size">
                    <h2>Отзывы</h2>
                    <a href="/otzyvy">Посмотреть все отзывы</a>
                    <div style="margin: 0 auto;"
                         class="metaslider metaslider-flex metaslider-actions-home ml-slider">
                        <div id="metaslider_container_interior_home">
                            <div id="metaslider_guestbook_home" class="flexslider" style="height: 500px;">
                                <ul class="slides" style="width: 1000%; transition-duration: 0s; transform: translate3d(-750px, 0px, 0px);">
                                    <?php foreach ($guestbook as $item): ?>
                                        <li style="display: block; width: 520px; float: left;" class="ms-image clone" aria-hidden="true">
                                            <span class="quote">“</span>
                                            <span class="meta">Пишет <?= $item->getName() ?>, <?= Yii::$app->formatter->asDatetime($item->time, 'short') ?></span>
                                            <span class="comment"><?= $item->getText() ?></span>
                                            <?php if ($item->getAnswer()) :?>
                                                <span class="meta">Ответ Администратора</span>
                                                <span class="comment"><?= $item->getAnswer() ?></span>
                                            <?php endif; ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <p><a class="btn btn-info btn-lg modal-window-button" href="#form-guestbook-modal">Оставить
                            отзыв</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end #guestbook -->
<?= $this->render('//guestbook/_modal', ['modalId' => 'form-guestbook-modal']); ?>
