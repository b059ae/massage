<?php
/** @var $asset yii\web\AssetBundle */
?>

<div id="about" class="white about-home white-home pattern text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Почему стоит работать у нас?</h2>
                <div class="row more-size">
                    <div class="row options text-left">
                        <div class="col-md-4 col-sm-6">
                            <h4>Зарплата: 90.000 - 250.000 рублей</h4>
                            <p class="icon">
                                <img width="70" height="70"
                                     src="/images/ico01.png"
                                     class="attachment-post-thumbnail wp-post-image">
                                Плюс все чаевые Вам! Ежедневные выплаты в конце смены.
                            </p>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <h4>Гибкий график</h4>
                            <p class="icon">
                                <img width="70" height="70"
                                     src="/images/ico11.png"
                                     class="attachment-post-thumbnail wp-post-image">
                                График составляется индивидуально. Это дает возможность
                                совмещение с учебой и другой занятостью.
                            </p>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <h4>Профессиональное обучение</h4>
                            <p class="icon">
                                <img width="70" height="70"
                                     src="/images/ico07.png"
                                     class="attachment-post-thumbnail wp-post-image">
                                Вы бесплатно пройдете тренинг по эротическому массажу стоимостью 40 000 руб.
                            </p>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <h4>Предоставление жилья</h4>
                            <p class="icon">
                                <img width="70" height="70"
                                     src="/images/ico08.png"
                                     class="attachment-post-thumbnail wp-post-image">
                                По вашему запросу мы предоставляем спальное место, шкафчик и уютную кухню.
                            </p>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <h4>Официальное трудоустройство</h4>
                            <p class="icon">
                                <img width="70" height="70"
                                     src="/images/ico12.png"
                                     class="attachment-post-thumbnail wp-post-image">
                                Мы можем официально оформить вас, указав любую должность в документах.
                            </p>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <h4>Доступ к SPA процедурам комплекса</h4>
                            <p class="icon">
                                <img width="70" height="70"
                                     src="/images/ico05.png"
                                     class="attachment-post-thumbnail wp-post-image">
                                В вашем распоряжении будет сауна, хамам и бассейн для отдыха.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
