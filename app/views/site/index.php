<?php
/**
 * @var $page \yii\easyii\modules\page\api\PageObject
 * @var $girls \yii\easyii\modules\catalog\api\ItemObject[]
 * @var $akcii \yii\easyii\modules\carousel\api\CarouselObject[]
 * @var $programmy \yii\easyii\modules\catalog\api\ItemObject[]
 * @var $interery \yii\easyii\modules\gallery\api\PhotoObject[]
 * @var $salony yii\easyii\modules\entity\api\ItemObject[]
 * @var $guestbook \yii\easyii\modules\guestbook\api\PostObject[]
 */
$asset = \app\assets\AppAsset::register($this);

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->seo('keywords')
]);
$this->title = $page->seo('title');

?>

<?= $this->render('slides/banner', ['asset' => $asset, 'salony' => $salony, 'page' => $page]); ?>
<?php /*
<?= $this->render('slides/open', ['asset' => $asset]); ?>
 */ ?>
<?= $this->render('slides/girls', ['asset' => $asset, 'girls' => $girls, 'title' => 'Мастера массажа']); ?>
<?= $this->render('slides/actions', ['asset' => $asset, 'akcii' => $akcii]); ?>
<?= $this->render('slides/services', ['asset' => $asset, 'programmy' => $programmy]); ?>
<?= $this->render('slides/guestbook', ['asset' => $asset, 'guestbook' => $guestbook]); ?>
<?= $this->render('slides/interior', ['asset' => $asset, 'interery' => $interery]); ?>
<?= $this->render('slides/about', ['asset' => $asset]); ?>
<?= $this->render('slides/map', ['asset' => $asset, 'salony' => $salony]); ?>
