<?php
use app\helpers\Html;
use yii\easyii\models\Setting;

/** @var $page \yii\easyii\modules\page\api\PageObject */

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->seo('keywords')
]);
$this->title = $page->seo('title');
$this->params['breadcrumbs'][] = $page->title;
?>
<section>
    <div>
        <div class="text-justify">
            <?= $page->text; ?>
        </div>
    </div>
</section>
