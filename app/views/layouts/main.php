<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Menu;

$appAsset = \app\assets\AppAsset::register($this);
$asset = \app\assets\FrontendAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <!--[if lt IE 7]>
    <html lang="ru-RU" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8 lt-ie7">
    <![endif]--><!--[if (IE 7)&!(IEMobile)]>
    <html lang="ru-RU" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8">
    <![endif]--><!--[if (IE 8)&!(IEMobile)]>
    <html lang="ru-RU" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9">
    <![endif]--><!--[if gt IE 8]><!-->
    <html prefix="og: http://ogp.me/ns#" class="js flexbox webgl no-touch geolocation hashchange history websockets rgba hsla multiplebgs backgroundsize borderimage textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage applicationcache svg svgclippaths mediaqueries no-regions supports" style="" lang="ru-RU">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" id="MobileOptimized" content="400">
        <meta name="viewport" id="viewport" content="initial-scale=1.0, maximum-scale=2, minimum-scale=1.0, user-scalable=yes, width=1903">

        <link rel="apple-touch-icon" href="/img/logo.png">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <meta name="msapplication-TileColor" content="#f01d4f">
        <meta name="msapplication-TileImage" content="/img/logo.png">

        <title><?= Html::encode($this->title) ?></title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Philosopher">

        <!--[if lt IE 9]>
        <link rel='stylesheet' id='bones-ie-only-css' href='/css/ie.css' type='text/css' media='all'/>
        <![endif]-->

        <?php $this->head() ?>
    </head>
    <body id="slug-home" class="home page page-id-2 page-template-default">
    <?php $this->beginBody() ?>
    <div class="body">
        <?= $this->render('_menu', ['asset' => $asset]); ?>
        <?= $content ?>
        <?= $this->render('_footer', ['asset' => $asset]); ?>
        <?php
        // Модальные окна. Рендерим в одном месте
        if (isset(Yii::$app->view->params['modals']) && is_array(Yii::$app->view->params['modals'])) {
            foreach (Yii::$app->view->params['modals'] as $modal) {
                echo $modal;
            }
        }
        ?>
        <?= $this->render('_callback'); ?>

        <!-- BEGIN JIVOSITE CODE {literal} -->
        <script type="text/javascript">
            (function (w, d) {
                w.amo_jivosite_id = '4SuyJ1UbwR';
                var s = document.createElement('script'), f = d.getElementsByTagName('script')[0];
                s.id = 'amo_jivosite_js';
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://forms.amocrm.ru/chats/jivosite/jivosite.js';
                f.parentNode.insertBefore(s, f);
            })(window, document);
        </script>
        <!-- {/literal} END JIVOSITE CODE -->
<?php /*
        <!--Обратный звонок-->
        <noindex><script async src="https://stats.lptracker.ru/code/new/34261"></script></noindex>
*/ ?>
    </div>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>