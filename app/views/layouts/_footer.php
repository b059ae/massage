<?php
/** @var $this \yii\web\View */
use yii\easyii\models\Setting;
use app\helpers\Html;

/** @var $asset \yii\web\AssetBundle */
?>
<footer id="footer" class="clearfix">
    <div id="footer-widgets">
        <div class="container">
            <div id="footer-wrapper">
                <div class="row">
                    <div class="col-sm-3 col-md-3">
                        <div id="nav_menu-2" class="widget widgetFooter widget_nav_menu">
                            <h4 class="widgettitle">Разделы</h4>
                            <div class="footer-menu-container">
                                <ul id="footer-menu" class="menu">
                                    <?php foreach (yii\easyii\modules\menu\api\Menu::items('main') as $item): ?>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page">
                                            <a title="Главная" href="<?=$item['url']?>"><?=$item['label']?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <div id="spice_core_salon-2" class="widget widgetFooter widget_spice_core_salon">
                            <h4 class="widgettitle">Контакты</h4>
                            <?php foreach (\yii\easyii\modules\entity\api\Entity::cat('salony')->getItems() as $item):?>
                                <ul class="menu salon-info">
                                    <li><?=$item->title?></li>
                                    <li class="address"><?=$item->addresstext?></li>
                                    <li class="hours">круглосуточно</li>
                                    <?php /*<li class="phone" id="zamena2"><?=$item->phone?></li> */ ?>
                                </ul>
                                <br/>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>