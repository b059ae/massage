<?php
/** @var $this \yii\web\View */
/** @var $asset \yii\web\AssetBundle */

use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;

?>
<?php if (isset($this->params['breadcrumbs']) && count($this->params['breadcrumbs'])): ?>
    <section>
        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => $this->params['breadcrumbs'],
            ]);
            ?>
        </div>
    </section>
<?php endif; ?>
