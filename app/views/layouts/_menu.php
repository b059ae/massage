<?php
/** @var $this \yii\web\View */
/** @var $asset \yii\web\AssetBundle */

use yii\helpers\Url;
use yii\widgets\Menu;

?>
<header class="header label18 pattern">
    <nav role="navigation">
        <div class="navbar">
            <div class="top-line">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="plus"><noindex>Интим-услуги не предоставляются!</noindex></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid text-center">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <?php /*<a class="navbar-brand hidden-sm" href="/" title="Kaprise" rel="homepage">
                        <img class="logo-image" src="/images/logo.png" alt="Hermitage Rooms"><img class="logo-image" src="/images/logo_kapriz.png" alt="Kaprise">
                        <img class="logo-thumb" src="/images/logo.png" alt="Hermitage Rooms">
                    </a>*/ ?>
                    <span id="phone"><a class="phone btn btn-info" href="tel:+79287773220" onclick="metrikaReachGoal('callback_menu'); return true;"><?php /*<div class="phone_text">Звони сейчас</div>*/ ?>8 928 777-32-20</a></span>
                    <div class="navbar-collapse collapse navbar-responsive-collapse">
                        <ul id="main-menu" class="nav navbar-nav navbar-right">
                            <?php foreach (yii\easyii\modules\menu\api\Menu::items('main') as $item): ?>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page">
                                    <a title="Главная" href="<?=$item['url']?>"><?=$item['label']?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>
<header class="header label18 sticky" style="display: none;">
    <nav role="navigation">
        <div class="navbar pattern">
            <div class="container-fluid text-center">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <?php /*<a class="navbar-brand hidden-sm" href="/" title="Kaprise" rel="homepage">
                        <img class="logo-image" src="/images/logo.png" alt="Kaprise"><img class="logo-image" src="/images/logo_kapriz.png" alt="Kaprise">
                        <img class="logo-thumb" src="/images/logo.png" alt="Kaprise">
                    </a>*/ ?>
                    <span id="phone-sticky"><a class="phone btn btn-info" href="tel:+79287773220" onclick="metrikaReachGoal('callback_menu'); return true;"><?php /*<div class="phone_text">Звони сейчас</div>*/ ?>8 928 777-32-20</a></span>
                    <div class="navbar-collapse collapse navbar-responsive-collapse">
                        <ul id="main-menu-sticky" class="nav navbar-nav navbar-right">
                            <?php foreach (yii\easyii\modules\menu\api\Menu::items('main') as $item): ?>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page">
                                    <a title="Главная" href="<?=$item['url']?>"><?=$item['label']?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter43689884 = new Ya.Metrika({
                    id:43689884,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
                <?php
                // Учет пользователей, которые перешли по ссылке из рассылки
                $status = null;
                if (isset(Yii::$app->request->cookies['status'])){
                    $status = Yii::$app->request->cookies['status']->value;
                }else{
                    if (isset($_GET['m'])){
                        $status = 'm';
                        Yii::$app->response->cookies->add(new \yii\web\Cookie([
                            'name' => 'status',
                            'value' => $status,
                        ]));
                    }
                }
                ?>
                <?php if ($status):?>
                w.yaCounter43689884.userParams({
                    status: "<?=$status?>"
                });
                <?php endif; ?>
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/43689884" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Цели -->
<script>
    function metrikaReachGoal(name, params){
        if (typeof yaCounter43689884 != "undefined") {
            if (typeof params == "undefined"){
                params = {};
            }
            yaCounter43689884.reachGoal(name, params);
        }
        /* ga('send', 'event', 'metrika', name); */
    }
    <!--  Применение событий -->
    <?php /*
    window.onload = function(){
        <?= Yii::$app->session->getFlash('metrika');?>
    }
    */ ?>
</script>
<!-- /Цели -->

<script type="text/javascript">
    var _cp = {trackerId: 36628};
    (function(d){
        var cp=d.createElement('script');cp.type='text/javascript';cp.async = true;
        cp.src='//tracker.cartprotector.com/cartprotector.js';
        var s=d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(cp, s);
    })(document);
</script>