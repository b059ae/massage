<?php

namespace app\helpers;

use app\models\CallbackForm;
use app\models\FeedbackForm;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

class Html extends \yii\helpers\Html
{

    /**
     * Удаляет не числа из строки
     * @param string $str
     * @return string
     */
    public static function removeNonNumeric($str)
    {
        return preg_replace("/[^0-9,.]/", "", $str);
    }

    /**
     * Не рендерим iframe по умолчанию при загрузке
     * @param  string $str
     * @return string
     */
    public static function stopIframe($str)
    {
        return preg_replace('/(<iframe[\s\S]+?)src=([\s\S]+?>[\s]*?<\/iframe>)/si', '$1data-src=$2', $str);
    }
}