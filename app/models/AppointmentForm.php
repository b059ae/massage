<?php

namespace app\models;

use yii\base\Model;
use yii\easyii\modules\catalog\api\Catalog;

class AppointmentForm extends Model
{
    public $name;
    public $phone;
    public $date;
    public $time;
    public $masseuse;

    public function rules()
    {
        return [
            [['phone'], 'required'],
            [['name','phone','date','time'], 'string'],
            [['masseuse'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Телефон',
            'date' => 'Дата',
            'time' => 'Время',
            'masseuse' => 'Массажистка',
        ];
    }

    public function masseuseList()
    {
        $catalog = Catalog::cat('devuski')->getItems([
            'filters' => ['working' => 1],
        ]);
        $names = [];
        foreach($catalog as $item) {
            $names[] = $item->title;
        }
        return $names;
    }

    public function save()
    {
    }
}
