<?php

namespace app\models;


use DateTime;

class Header
{
    /**
     * Будний день с 10 до 20
     */
    const WEEKDAY_MORNING = 1;
    /**
     * Будний день с 12 до 18
     */
    const WEEKDAY_LAUNCH = 2;
    /**
     * Будний день с 20 до 10
     */
    const WEEKDAY_NIGHT = 3;
    /**
     * Выходные дни
     */
    const WEEKEND = 4;

    /**
     * Слоган оффера
     *
     * @return string
     */
    public static function getSlogan()
    {
        // Слоган по Акции
        switch (self::getStockType()) {
            case self::WEEKDAY_MORNING:
                return 'Звони сейчас! Программа <span class="marked">"Бизнес экспресс"</span> - 40 минут удовольствия <span class="marked">за 1999р.</span> <s>2500р.</s>'; // <span class="marked">+ 10 мин. в подарок</span>
            case self::WEEKDAY_LAUNCH:
                return 'Звони сейчас и получи <span class="marked">скидку 30%</span> на любую массажную программу!';
            case self::WEEKDAY_NIGHT:
                return 'Звони сейчас! Акция <span class="marked">15 мин бесплатно</span> или <span class="marked">15% Депозит на бар</span>!';
            case self::WEEKEND:
                return 'Акция! Получи <span class="marked">наслаждение от двух девушек всего за 3500р.</span> Скидка 15% на программы с двумя девушками!';
            default:
                break;
        }

        // Значение по умолчанию
        return 'Сексуальные девушки, владеющие всеми техниками СПА и эротического релакса, уже ждут вас!';
    }

    /**
     * Количество секунд до завершения акции
     *
     * @return int
     */
    public static function getEndTime()
    {
        // Количество секунд до завершения акции
        $timezone = new \DateTimeZone('Europe/Moscow');
        switch (self::getStockType()) {
            case self::WEEKDAY_MORNING:
                $diff = new DateTime(date('Y-m-d', strtotime('now')) . ' 20:00', $timezone);
                break;
            case self::WEEKDAY_LAUNCH:
                $diff = new DateTime(date('Y-m-d', strtotime('now')) . ' 18:00', $timezone);
                break;
            case self::WEEKDAY_NIGHT:
                $diff = new DateTime(date('Y-m-d', strtotime('+1 day')) . ' 10:00', $timezone);
                break;
        }
        // Значение по умолчанию
        if (isset($diff)) {
            $current = new DateTime('now', $timezone);
            return self::toSeconds($current->diff($diff));
        }
        return 0;
    }

    /**
     * Получение возможного типа акции
     *
     * @return int
     */
    public static function getStockType()
    {
        $current = new DateTime('now', new \DateTimeZone('Europe/Moscow'));
        $weekDay = $current->format('N');
        // Будни
        if ($weekDay <= 5) {
            if (static::isBetween('10:00', '20:00', $current->format('H:i'))) {
                // C 12 до 18 с Пн по Чт
                if ($weekDay < 5 && static::isBetween('12:00', '18:00', $current->format('H:i'))) {
                    return self::WEEKDAY_LAUNCH;
                }
                // c 10 до 20
                return self::WEEKDAY_MORNING;
            }
            // c 20 до 10
            return self::WEEKDAY_NIGHT;
        // Выходные
        } else {
            return self::WEEKEND;
        }
    }

    /**
     * Получение разницы времени в секундах
     *
     * @param \DateInterval $diff
     * @return mixed
     */
    public static function toSeconds($diff)
    {
        return  ($diff->h * 60  * 60) +
                ($diff->i * 60) +
                 $diff->s;
    }

    /**
     * Проверка на вхождение времени в промежуток
     *
     * @param string $from
     * @param string $till
     * @param string $input
     * @return bool
     */
    public static function isBetween($from, $till, $input)
    {
        $f = DateTime::createFromFormat('!H:i', $from);
        $t = DateTime::createFromFormat('!H:i', $till);
        $i = DateTime::createFromFormat('!H:i', $input);
        if ($f > $t) $t->modify('+1 day');
        return ($f <= $i && $i <= $t) || ($f <= $i->modify('+1 day') && $i <= $t);
    }
}
