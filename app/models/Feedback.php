<?php
/**
 * Created by PhpStorm.
 * User: b059a
 * Date: 28.03.2017
 * Time: 20:07
 */

namespace app\models;


use app\helpers\Html;

class Feedback extends \yii\easyii\modules\feedback\models\Feedback
{
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert){
            $this->callback();
        }
    }

    public function callback()
    {
        $phone = Html::removeNonNumeric($this->phone);
        \Yii::$app->megafon->makeCall($phone);

    }
}
