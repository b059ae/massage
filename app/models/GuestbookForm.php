<?php

namespace app\models;

use app\validators\NameMatchValidator;
use yii\base\Model;
use yii\easyii\modules\guestbook\models\Guestbook;

class GuestbookForm extends Model
{
    public $name;
    public $text;

    public function rules()
    {
        return [
            [['name', 'text'], 'required'],
            [['name'], NameMatchValidator::class],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Ваше имя',
            'text' => 'Ваш отзыв',
        ];
    }

    public function save()
    {
        if (!$this->validate()){
            return false;
        }
        // Обработчик сохранения
        $model = new Guestbook([
            'name' => $this->name,
            'text' => $this->text,
        ]);

        return $model->save();
    }
}
