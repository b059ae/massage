(function ($) {
    $(document).ready(function () {
        $('#top-line').detach().insertBefore($('header.header .navbar .container'));

        // background video
        /*$("body > .body").tubular({videoId: "R6g2Ql4gpJ8", start: 10});*/

        // colorbox
        $(".overlay-image").colorbox({rel: "overlay-image"});
        $(".overlay-video").colorbox({iframe:true, innerWidth:640, innerHeight:390});
        $(".overlay-modal").colorbox({inline:true, width:"50%", height: "90%"});

        // all girls
        $("#all-girls-show").on("click", function (e) {
            e.preventDefault();
            var girls = $("#all-girls");
            var th = $(this);
            if (girls.hasClass("active")) {
                girls.slideUp().removeClass("active");
                $('html,body').animate({
                    scrollTop: $("#girls").offset().top
                }, 1000);
                th.text("Все девушки");
            } else {
                girls.slideDown().addClass("active");
                $('html,body').animate({
                    scrollTop: th.offset().top
                }, 1000, function () {
                    // Animation complete.
                    th.text("Скрыть");
                });
            }
        });
        $("#more-services-show").on("click", function (e) {
            e.preventDefault();
            var services = $("#more-services");
            if (services.hasClass("active")) {
                services.slideUp().removeClass("active");
            } else {
                services.slideDown().addClass("active");
            }
        });

        // scale
        var mw = $('#MobileOptimized').attr('content');
        if (mw <= 520) {
            var lw = 0;
            function setViewport() {
                var ww = $(window).width() < window.screen.width ? $(window).width() : window.screen.width;
                if (ww == lw) return;
                lw = ww;
                if (ww < mw) {
                    var ratio = ww / mw;
                    $('#viewport').attr('content', 'initial-scale=' + ratio + ', maximum-scale=' + ratio + ', minimum-scale=' + ratio + ', user-scalable=yes, width=' + ww);
                } else {
                    $('#viewport').attr('content', 'initial-scale=1.0, maximum-scale=2, minimum-scale=1.0, user-scalable=yes, width=' + ww);
                }
            }
            $(window).resize(setViewport);
            setViewport();
        }

        // sticky menu
        var sticky_menu_selector = 'header.header';
        $(sticky_menu_selector).clone().addClass('sticky').insertAfter(sticky_menu_selector);
        function stickIt() {
            var min_top = $(sticky_menu_selector).outerHeight();
            if ($(window).scrollTop() >= min_top) {
                $(sticky_menu_selector + '.sticky').fadeIn();
            } else {
                $(sticky_menu_selector + '.sticky').hide();
            }
        }
        setInterval(stickIt, 10);

        // scroll
        function scrollToAnchor(hash) {
            target = $(hash);
            if (!target.length) target = $('[name=' + hash.slice(1) + ']');
            if (!target.length) return false;
            $('html,body').animate({scrollTop: target.offset().top}, 1000);
            return false;
        }
        $(".scroll").on("click", function () {
            var curr_url = location.hostname + location.pathname.replace(/^\//, '');
            var link_url = this.hostname + this.pathname.replace(/^\//, '');
            if (curr_url == link_url) {
                return scrollToAnchor(this.hash);
            }
        });
        if (window.location.hash) {
            scrollToAnchor(window.location.hash);
        }

        // banner
        var banner_size = $('#banner .banner-size');
        //var video_frame = $("#video-autosize");
        function setHomeBannerSize() {
            var w = $('#banner').width();
            switch (true) {
                case w >= 1360:
                    banner_size.css('min-height', 680);
                    break;
                case w >= 680:
                    banner_size.css('min-height', w / 2);
                    break;
                case w >= 320:
                    banner_size.css('min-height', 340);
                    break;
                default:
                    banner_size.css('min-height', w / 0.941176471);
                    break;
            }
            //video_frame.css('height', video_frame.width() * 0.60);
        }
        /*function setBannerSize() {
            var w = $('#banner').width();
            switch (true) {
                case w >= 1360:
                    banner_size.css('height', 340);
                    break;
                case w >= 680:
                    banner_size.css('height', w / 4);
                    break;
                case w >= 320:
                    banner_size.css('height', 170);
                    break;
                default:
                    banner_size.css('height', w / 1.882352941);
                    break;
            }
        }*/

        //if ($('#banner.banner-home').length) {
            $(window).resize(function () {
                setHomeBannerSize()
            });
            setHomeBannerSize();
        /*} else {
            $(window).resize(function () {
                setBannerSize()
            });
            setBannerSize();
        }*/

        var stockTimeoutElement = $("#stock-timeout");
        if (stockTimeoutElement.length > 0) {
            var stockH = stockTimeoutElement.find("#stock-timeout-h");
            var stockM = stockTimeoutElement.find("#stock-timeout-m");
            var stockS = stockTimeoutElement.find("#stock-timeout-s");
            function stockTimeout() {
                var timeout = stockTimeoutElement.data("timeout") - 1;
                var h = ("0" + Math.floor(timeout / 3600 % 24)).slice(-2);
                var m = ("0" + Math.floor(timeout / 60 % 60)).slice(-2);
                var s = ("0" + Math.floor(timeout % 60)).slice(-2);
                if (stockH.text() != h) {
                    stockH.text(h);
                }
                if (stockM.text() != m) {
                    stockM.text(m);
                }
                stockS.text(s);
                stockTimeoutElement.data("timeout", timeout);
            }
            stockTimeout();
            var stockInterval = setInterval(stockTimeout, 1000);
        }

        // girls images proportions
        /*function setGirlImageSize() {
            var w = 0;
            $.each($('.girl .girl-wrapper'), function (i, el) {
                w += $(el).parent().width();
            });
            $('.girl .girl-wrapper').css('height', w * 1.5 / 4).css('max-height', 'none');
        }
        $(window).resize(setGirlImageSize);
        setGirlImageSize();*/

        // slider
        $('.flex-viewport .slides li img').click(function () {
            $('.flex-direction-nav li a.flex-next').click();
        });

        //Слайдер выгодных предложений
        var slider = $('#metaslider_actions_home').flexslider({
            controlNav:true,
            directionNav:true,
            animation:"slide",
            animationSpeed:600,
            reverse:false,
            slideshow:true,
            slideshowSpeed:5000,
            pauseOnHover:true,
            direction:"horizontal",
            prevText:"&lt;",
            easing:"linear",
            nextText:"&gt;"
        });
        var last_slider_width = 0;
        var metaslider_resize = function(){
            if (last_slider_width == slider.width()) {
                return;
            }
            last_slider_width = slider.width();
            slider.height(slider.width() * 2/3);
            $('#metaslider_actions_home li img').height(slider.height());
            slider.resize();
        };
        $(window).resize(metaslider_resize);
        metaslider_resize();

        //Слайдер интерьеров
        var slider = $('#metaslider_interior_home').flexslider({
            controlNav:true,
            directionNav:true,
            animation:"slide",
            animationSpeed:600,
            reverse:false,
            slideshow:true,
            slideshowSpeed:5000,
            pauseOnHover:true,
            direction:"horizontal",
            prevText:"&lt;",
            easing:"linear",
            nextText:"&gt;"
        });
        var last_slider_width = 0;
        var metaslider_resize = function(){
            if (last_slider_width == slider.width()) {
                return;
            }
            last_slider_width = slider.width();
            slider.height(slider.width() * 2/3);
            $('#metaslider_interior_home li img').height(slider.height());
            slider.resize();
        };
        $(window).resize(metaslider_resize);
        metaslider_resize();

        //Слайдер отзывов
        var slider = $('#metaslider_guestbook_home').flexslider({
            controlNav:true,
            directionNav:true,
            animation:"slide",
            animationSpeed:600,
            reverse:false,
            slideshow:true,
            slideshowSpeed:5000,
            pauseOnHover:true,
            direction:"horizontal",
            prevText:"&lt;",
            easing:"linear",
            nextText:"&gt;"
        });
        var last_slider_width = 0;
        var metaslider_resize = function(){
            if (last_slider_width == slider.width()) {
                return;
            }
            last_slider_width = slider.width();
            /*slider.height(slider.width() * 2/3);
            slider.resize();*/
        };
        $(window).resize(metaslider_resize);
        metaslider_resize();

        // Модальные окна
        if ($(".modal-window-wrapper").length > 0) {
            function showModalWindow(id) {
                var target = $(id);
                if (target.length > 0) {
                    target.fadeIn(250);
                }
            }
            $(document).on("click", ".modal-window-button", function (e) {
                e.preventDefault();
                showModalWindow($(this).attr("href"));
            });
            $(document).on("click", ".modal-window-close", function (e) {
                e.preventDefault();
                var parent = $(this).closest(".modal-window-wrapper");
                if (parent.length) {
                    $(document).trigger("closeModalWindow", ["#" + parent.attr("id")]);
                }
            });
            $(document).on("click", ".modal-window-wrapper", function(e) {
                var target = $(e.target);
                if (target.hasClass("modal-window-wrapper")) {
                    target.fadeOut(250);
                }
            });
            $(document).on("showModalWindow", function (e, id) {
                showModalWindow(id);
            });
            $(document).on("closeModalWindow", function (e, id) {
                var target = $(id);
                if (target.length > 0) {
                    target.fadeOut(250);
                }
            });
        }

    });
})(jQuery);
