<?php

namespace app\components\megafon;

use Yii;
use yii\base\Component;

/**
 * Class
 * Компонент для взаимодействия с телефонией мегафона
 * @package app\components
 */
class Megafon extends Component
{
    /**
     * URL для вызова
     * @var string
     */
    public $url;
    /**
     * Пользователь которому звонить
     * @var array
     */
    public $user;
    /**
     * Токен
     * @var string
     */
    public $token;

    /**
     * Выполняем обратный звонок
     * @param $phone
     */
    public function makeCall($phone)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, [
            'cmd' => 'makeCall',
            'user' => $this->user,
            'phone' => $phone,
            'token' => $this->token,
        ]);
        $out = curl_exec($curl);
        curl_close($curl);
    }

}