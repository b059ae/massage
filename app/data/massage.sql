-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Время создания: Май 30 2017 г., 20:56
-- Версия сервера: 5.6.33-79.0
-- Версия PHP: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `u0193363_hrooms`
--

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_admins`
--

DROP TABLE IF EXISTS `easyii_admins`;
CREATE TABLE `easyii_admins` (
  `id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `auth_key` varchar(128) DEFAULT NULL,
  `access_token` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_article_categories`
--

DROP TABLE IF EXISTS `easyii_article_categories`;
CREATE TABLE `easyii_article_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_article_items`
--

DROP TABLE IF EXISTS `easyii_article_items`;
CREATE TABLE `easyii_article_items` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `image_file` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_carousel`
--

DROP TABLE IF EXISTS `easyii_carousel`;
CREATE TABLE `easyii_carousel` (
  `id` int(11) NOT NULL,
  `image_file` varchar(128) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(1024) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_carousel`
--

INSERT INTO `easyii_carousel` (`id`, `image_file`, `link`, `title`, `text`, `order_num`, `status`) VALUES
(2, 'carousel/photo03-d7f5be8c76.jpg', '', 'Бизнес-экспресс за&nbsp;1999&nbsp;<s>2500</s>&nbsp;Р. + 10 мин. в подарок', 'С понедельника по пятницу с 10:00 до 20:00', 2, 1),
(3, 'carousel/20161101205948-9ecac32db0-092ecf2eca.jpg', '', 'Кальян в подарок + 30 минут романтика', 'На заказы от 6000 Р. с понедельника по пятницу с 10:00 до 20:00', 3, 0),
(4, 'carousel/kim8084-bba29cfb6c.jpg', '', 'скидка 15% на программы с двумя девушками', 'В субботу и воскресенье', 4, 1),
(5, 'carousel/kapriznoth5-f939a4edcf.jpg', '', '+15 минут времени или 15% депозит на бар', 'С понедельника по пятницу с 20:00 до 10:00', 5, 1),
(6, 'carousel/kim7652-e6ca852ada.jpg', '', 'Массаж стоп в подарок, добавка продлевает программу на 10 минут', 'С понедельника по пятницу с 10:00 до 20:00', 6, 1),
(7, 'carousel/57061f577955bsofia31-8a4b711d82.jpg', '', 'Сертификат на 2000&nbsp;Р. в подарок при покупке программы от 6000&nbsp;Р.', 'В салоне Каприз Северный', 7, 1),
(8, 'carousel/57061f7bafc88kapriz4op-19c3663957.jpg', '', 'Скидка 30% на все программы кроме Бизнес-экспресс', 'В салонах Каприз Западный и Эрмитаж Румс с 12:00 до 18:00', 8, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_catalog_categories`
--

DROP TABLE IF EXISTS `easyii_catalog_categories`;
CREATE TABLE `easyii_catalog_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `fields` text,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_catalog_categories`
--

INSERT INTO `easyii_catalog_categories` (`id`, `title`, `description`, `image_file`, `fields`, `slug`, `tree`, `lft`, `rgt`, `depth`, `order_num`, `status`) VALUES
(1, 'Девушки', '', NULL, '[{"name":"height","title":"\\u0420\\u043e\\u0441\\u0442","type":"string","options":""},{"name":"thigh","title":"\\u041e\\u0431\\u044a\\u0435\\u043c \\u0431\\u0435\\u0434\\u0435\\u0440","type":"string","options":""},{"name":"breast","title":"\\u0413\\u0440\\u0443\\u0434\\u044c","type":"string","options":""},{"name":"working","title":"\\u0420\\u0430\\u0431\\u043e\\u0442\\u0430\\u0435\\u0442","type":"boolean","options":""}]', 'devuski', 1, 1, 2, 0, 1, 1),
(2, 'Программы', NULL, NULL, '{}', 'programmy', 2, 1, 2, 0, 2, 1),
(3, 'Дополнения', NULL, NULL, '{}', 'dopolnenia', 3, 1, 2, 0, 3, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_catalog_items`
--

DROP TABLE IF EXISTS `easyii_catalog_items`;
CREATE TABLE `easyii_catalog_items` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `available` int(11) DEFAULT '1',
  `price` float DEFAULT '0',
  `discount` int(11) DEFAULT '0',
  `data` text,
  `image_file` varchar(128) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_catalog_items`
--

INSERT INTO `easyii_catalog_items` (`id`, `category_id`, `title`, `description`, `available`, `price`, `discount`, `data`, `image_file`, `slug`, `time`, `status`) VALUES
(1, 1, 'Алена', '<p>Загадочная шатенка с карими глазами</p>', 1, NULL, NULL, '{"height":"169","thigh":"95","breast":"1","working":"1"}', 'catalog/0g2a6737-l-9649c5390f.jpg', 'alena', 1490996364, 1),
(2, 2, 'Программа «Босс лайф эксклюзив»', '<p>Это не массажная программа. Здесь вы покупаете время, которое \r\nпроведете в нашем заведении. В этой программе вы можете воспользоваться \r\nвсеми видами пикантных добавок и эротических услуг нашего массажного \r\nсалона в Ростове.\r\n</p><p>Это самая дорогая программа, которая позволяет воплотить в реальность самые тайные и максимально эротические фантазии.\r\n</p><p>Добавки <strong>«Босс лайф эксклюзив»</strong>:\r\n</p><ul><li>Купание до и после массажа, </li><li>массаж всего тела,</li><li>аквабоди,<span class="redactor-invisible-space"></span></li><li><span class="redactor-invisible-space">веточка сакуры,<span class="redactor-invisible-space"></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space">горячие апельсины,<span class="redactor-invisible-space"></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">тайская трапеза,<span class="redactor-invisible-space"></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">лесби-шоу откровенное<span class="redactor-invisible-space"></span></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">пип-шоу эксклюзив,</span></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">боди массаж,<span class="redactor-invisible-space"></span></span></span></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">ваша партия,</span></span></span></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">экстра эгоист,<span class="redactor-invisible-space"></span></span></span></span></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">небесные ножки,<span class="redactor-invisible-space"></span></span></span></span></span></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">купание красной лилии,<span class="redactor-invisible-space"></span></span></span></span></span></span></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">сладкое ощущение,<span class="redactor-invisible-space"></span></span></span></span></span></span></span></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">высший пилотаж,<span class="redactor-invisible-space"></span></span></span></span></span></span></span></span></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">золотой дождь,</span></span></span></span></span></span></span></span></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">игрушка,</span></span></span></span></span></span></span></span></span></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">аквапенное удовольствие,</span></span></span></span></span></span></span></span></span></span></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">массаж пикантный,<span class="redactor-invisible-space"></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space">приватный танец,</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></li><li><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"><span class="redactor-invisible-space"> горячий тест\r\n<br></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></li></ul><p> Количество расслаблений не ограничено.\r\n</p><p><strong>Цена:</strong>\r\n	<br> 1 девушка — 9 000 руб.\r\n	<br> 2 девушки — 15 000 руб.\r\n</p><p><strong>Длительность:</strong> <em>1 ч 10 мин</em>\r\n</p><iframe src="https://www.youtube.com/embed/eoDtd7XJ7WM" allowfullscreen="" frameborder="0" height="315" width="100%">\r\n</iframe><iframe src="https://www.youtube.com/embed/cDgIpQ5tKSY" allowfullscreen="" frameborder="0" height="315" width="100%">\r\n</iframe>', 1, 9000, NULL, '{}', NULL, 'programma-boss-lajf-ekskluziv', 1489345253, 1),
(3, 2, 'Программа «Эгоист»', '<p>Это наполненная массажная программа длительностью один час сорок \r\nминут, девушка искупает вас и выполнит \r\n	<strong>аква-боди</strong> «первое расслабление в \r\nдуше», затем массаж от колен до предплечий и второе расслабление, а в \r\nзавершение – \r\n	<strong>боди-массаж</strong>, <strong>веточка сакуры</strong> и третье расслабление.\r\n</p>\r\n<p>Прикосновения к телу девушки разрешены.\r\n</p>\r\n<p><strong>Цена:</strong>\r\n	<br> 1 девушка — 8 000 руб.\r\n	<br> 2 девушки — 11 000 руб.\r\n</p>\r\n<p><strong>Длительность:</strong> <em>1 ч 40 мин</em>\r\n</p>\r\n<iframe width="100%" height="315" src="https://www.youtube.com/embed/20_rcbMZ3Pk" frameborder="0" allowfullscreen></iframe>\r\n<iframe width="100%" height="315" src="https://www.youtube.com/embed/cDgIpQ5tKSY" frameborder="0" allowfullscreen></iframe>\r\n', 1, 8000, NULL, '{}', NULL, 'programma-egoist', 1489345273, 1),
(4, 2, 'Программа «Леди боди»', '<p>Программа без массажа. Девушка искупает вас в душе и расслабит используя добавку <strong>аквабоди</strong>, в массажной комнате покроет тело поцелуями, затем выполнит <strong>пип-шоу</strong> театральное (гладит и ласкает себя) и второе расслабление техникой лингам (девушку трогать можно).\r\n</p><p><strong>Цена:</strong> <br> 1 девушка — 5 000 руб. <br> 2 девушки — 8 000 руб.\r\n</p><p><strong>Длительность:</strong> <em>60 мин</em>\r\n</p><iframe src="https://www.youtube.com/embed/20_rcbMZ3Pk" allowfullscreen="" frameborder="0" height="315" width="100%"></iframe><iframe src="https://www.youtube.com/embed/iBAuQIEUzNM" allowfullscreen="" frameborder="0" height="315" width="100%"></iframe>', 1, 5000, NULL, '{}', NULL, 'programma-ledi-bodi', 1489345336, 1),
(5, 2, 'Программа «Баунти»', '<p>Быстрая программа. Девушка искупает вас, сделает массаж горячими \r\nапельсинами (интересные ощущения, кажется, что это массаж грудью), \r\nсделает \r\n	<strong>боди-массаж</strong> (телом на масле) и расслабит. Можно работать дальше без эротических мыслей. Здесь советуем взять добавку (<strong>ваша партия</strong>), чтобы можно было трогать девушку.\r\n</p>\r\n<p><strong>Цена:</strong>\r\n	<br> 1 девушка — 3 500 руб.\r\n	<br> 2 девушки — 4 500 руб.\r\n</p>\r\n<p><strong>Длительность:</strong> <em>45 мин</em>\r\n</p>\r\n<iframe width="100%" height="315" src="https://www.youtube.com/embed/eM0dyJUG30s" frameborder="0" allowfullscreen></iframe>\r\n<iframe width="100%" height="315" src="https://www.youtube.com/embed/XiqcIOQArBA" frameborder="0" allowfullscreen></iframe>', 1, 3500, NULL, '{}', NULL, 'programma-baunti', 1489345357, 1),
(6, 2, 'Программа «Спа релакс дуэт»', '<p>Это наполненная спа-программа для двоих гостей с двумя мастерами, \r\nпродолжительностью два часа тридцать минут. Программа выполняется в бане\r\n или хаммаме на ваш выбор.<br></p><p>По желанию гостей массажная программа может выполняться одновременно в одной массажной комнате.<span class="redactor-invisible-space"></span> \r\nНаполняемость:\r\n</p><ul>\r\n	<li>Купание до и после массажа, </li>\r\n	<li>массаж всего тела,</li>\r\n	<li>аквабоди,</li>\r\n	<li>веточка сакуры,</li>\r\n	<li>горячие апельсины,</li>\r\n	<li>тайская трапеза,</li>\r\n	<li>лесби шоу театральное,</li>\r\n	<li>пип-шоу театральное,</li>\r\n	<li>боди массаж,</li>\r\n	<li>ваша партия,</li>\r\n	<li>экстра эгоист,</li>\r\n	<li>небесные ножки,</li>\r\n	<li>купание красной лилии,</li>\r\n	<li>сладкое ощущение,</li>\r\n	<li>высший пилотаж,</li>\r\n	<li>аквапенное удовольствие,</li>\r\n	<li>массаж пикантный,</li>\r\n	<li>приватный танец,</li>\r\n	<li>горячий тест,</li>\r\n	<li>скрабирование всего тела,</li>\r\n	<li>парилка с веником,</li>\r\n	<li>чайная церемония</li>\r\n</ul><p "="">По желанию гостя <strong>пилинг стоп</strong> в подарок.\r\n</p><p>Количество расслаблений не ограничено.\r\n</p><p>Прикосновения к телу девушки разрешены.\r\n</p><p><strong>Цена:</strong>\r\n				\r\n				<br> 2 мастера — 23 000 руб.\r\n					\r\n				</p><p><strong>Длительность:</strong> <em>2 ч 30 мин</em></p><iframe src="https://www.youtube.com/embed/eoDtd7XJ7WM" allowfullscreen="" frameborder="0" height="315" width="100%"></iframe><iframe src="https://www.youtube.com/embed/K3crNvKPJcM" allowfullscreen="" frameborder="0" height="315" width="100%"></iframe>', 1, 23000, NULL, '{}', NULL, 'programma-spa-relaks-duet', 1489344355, 1),
(7, 2, 'Программа «Спа релакс для одного»', '<p>Это наполненная спа-программа продолжительностью два часа тридцать \r\nминут. Программа выполняется в бане или хаммаме на ваш выбор. \r\nНаполняемость:\r\n</p><ul>\r\n	<li>Купание до и после массажа, </li>\r\n	<li>массаж всего тела,</li>\r\n	<li>аквабоди,</li>\r\n	<li>веточка сакуры,</li>\r\n	<li>горячие апельсины,</li>\r\n	<li>тайская трапеза,</li>\r\n	<li>лесби шоу театральное,</li>\r\n	<li>пип-шоу театральное,</li>\r\n	<li>боди массаж,</li>\r\n	<li>ваша партия,</li>\r\n	<li>экстра эгоист,</li>\r\n	<li>небесные ножки,</li>\r\n	<li>купание красной лилии,</li>\r\n	<li>сладкое ощущение,</li>\r\n	<li>высший пилотаж,</li>\r\n	<li>аквапенное удовольствие,</li>\r\n	<li>массаж пикантный,</li>\r\n	<li>приватный танец,</li>\r\n	<li>горячий тест,</li>\r\n	<li>скрабирование всего тела,</li>\r\n	<li>парилка с веником,</li>\r\n	<li>чайная церемония</li>\r\n</ul><p>По желанию гостя <strong>пилинг стоп</strong> в подарок.\r\n</p><p>Количество расслаблений не ограничено.\r\n</p><p>Прикосновения к телу девушки разрешены.\r\n</p><p><strong>Цена:</strong>\r\n	<br> 1 девушка — 13 000 руб.\r\n	<br> 2 девушки — 19 000 руб.\r\n</p><p><strong>Длительность:</strong> <em>2 ч 30 мин</em>\r\n</p><iframe src="https://www.youtube.com/embed/eoDtd7XJ7WM" allowfullscreen="" frameborder="0" height="315" width="100%">\r\n</iframe><iframe src="https://www.youtube.com/embed/K3crNvKPJcM" allowfullscreen="" frameborder="0" height="315" width="100%"></iframe>', 1, 13000, NULL, '{}', NULL, 'programma-spa-relaks-dla-odnogo', 1489345223, 1),
(8, 2, 'Программа «Босс лайф»', '<p>Это не массажная программа. Здесь вы покупаете время, которое проведете в нашем заведении.\r\n</p><p>По вашему желанию программа может быть максимально эротичной и состоять из практически всех наших добавок. Вы говорите, что бы вы хотели получить в массажной комнате, а администратор сообщит вам, сколько потребуется времени на выполнение собранной вами программы.\r\n</p><p "="">Добавки <strong>«Босс лайф»</strong>:\r\n</p><ul>\r\n	<li>Купание до и после массажа, </li>\r\n	<li>массаж всего тела,</li>\r\n	<li>аквабоди,</li>\r\n	<li>веточка сакуры,</li>\r\n	<li>горячие апельсины,</li>\r\n	<li>тайская трапеза,</li>\r\n	<li>лесби шоу театральное,</li>\r\n	<li>пип-шоу театральное,</li>\r\n	<li>боди массаж,</li>\r\n	<li>ваша партия,</li>\r\n	<li>экстра эгоист,</li>\r\n	<li>небесные ножки,</li>\r\n	<li>купание красной лилии,</li>\r\n	<li>сладкое ощущение,</li>\r\n	<li>высший пилотаж</li>\r\n</ul><p>Количество расслаблений не ограничено.\r\n</p><p><strong>Цена:</strong> <br> 1 девушка — 6 000 руб. <br>2 девушки — 9 000 руб.\r\n</p><p><strong>Длительность:</strong> <em>1 ч</em>\r\n</p><iframe src="https://www.youtube.com/embed/20_rcbMZ3Pk" allowfullscreen="" frameborder="0" height="315" width="100%"></iframe><iframe src="https://www.youtube.com/embed/eM0dyJUG30s" allowfullscreen="" frameborder="0" height="315" width="100%"><span id="selection-marker-1" class="redactor-selection-marker"></span></iframe>', 1, 6000, NULL, '{}', NULL, 'programma-boss-lajf', 1489345319, 1),
(9, 2, 'Программа «Босс лайф плюс»', '<p "="">Это не массажная программа. Здесь вы покупаете время, которое проведете в нашем заведении. В этой программе вы можете воспользоваться всеми добавками <strong>«Босс лайф плюс»</strong>:\r\n</p>\r\n<ul>\r\n	<li>Купание до и после массажа, </li>\r\n	<li>массаж всего тела,</li>\r\n	<li>аквабоди,</li>\r\n	<li>веточка сакуры,</li>\r\n	<li>горячие апельсины,</li>\r\n	<li>тайская трапеза,</li>\r\n	<li>лесби шоу театральное,</li>\r\n	<li>пип-шоу театральное,</li>\r\n	<li>боди массаж,</li>\r\n	<li>ваша партия,</li>\r\n	<li>экстра эгоист,</li>\r\n	<li>небесные ножки,</li>\r\n	<li>купание красной лилии,</li>\r\n	<li>сладкое ощущение,</li>\r\n	<li>высший пилотаж,</li>\r\n	<li>аквапенное удовольствие,</li>\r\n	<li>массаж пикантный,</li>\r\n	<li>приватный танец,</li>\r\n	<li>горячий тест</li>\r\n</ul>\r\n<p><strong></strong>\r\n</p>\r\n<p><br><strong>Цена:</strong> <br>1 девушка — 7 000 руб. <br>2 девушки — 10 000 руб. <br>3 девушки — 13 000 руб. <br>4 девушки — 16 000 руб.\r\n</p>\r\n<p><strong>Длительность:</strong> <em>1 ч 10 мин</em>\r\n</p>\r\n<iframe width="100%" height="315" src="https://www.youtube.com/embed/eoDtd7XJ7WM" frameborder="0" allowfullscreen></iframe>\r\n<iframe width="100%" height="315" src="https://www.youtube.com/embed/Jw8jD82rfVA" frameborder="0" allowfullscreen></iframe>', 1, 7000, NULL, '{}', NULL, 'programma-boss-lajf-plus', 1489345286, 1),
(10, 3, '«Аквапенное удовольствие»', '<p>Боди массаж в джакузи.\r\n</p>\r\n<p>Увеличивает время любой программы на 15-20 минут\r\n</p>\r\n<p><strong>1 девушка</strong> — 1 800 руб.<br><strong>2 девушки</strong> — 2 300 руб.\r\n</p>\r\n<iframe width="100%" height="315" src="https://www.youtube.com/embed/eoDtd7XJ7WM" frameborder="0" allowfullscreen></iframe>', 1, 7000, NULL, '{}', NULL, 'akvapennoe-udovolstvie', 1489345972, 1),
(11, 3, '«Аквабоди»', '<p>Расслабление в душе.</p><p>Увеличивает время любой программы на 15 минут.</p><p><strong>1 девушка</strong> — 1 800 руб.<br><strong>2 девушки</strong> — 2 300 руб.</p>', 1, 7000, NULL, '{}', NULL, 'akvabodi', 1489346051, 1),
(12, 3, '«Горячий тест»', '<p>Горячий тест - сделать девушке приятно</p><p>Увеличивает время любой программы до 15 мин</p><p><strong>Стоимость</strong> — 1800 руб.</p>', 1, 7000, NULL, '{}', NULL, 'goracij-test', 1489346070, 1),
(13, 3, '«Высший пилотаж»', '<p>Расслабление телом девушки: ягодицами, шеей и грудью</p><p><strong>Стоимость</strong> — 900 руб.</p>', 1, 7000, NULL, '{}', NULL, 'vyssij-pilotaz', 1489346084, 1),
(14, 3, '«Купание красного коня»', '<p>Купание гостя</p><p><strong>Бесплатно</strong></p>', 1, 7000, NULL, '{}', NULL, 'kupanie-krasnogo-kona', 1489346104, 1),
(15, 3, '«Горячие апельсины»', '<p>Массаж горячими апельсинами.</p><p>Увеличивает время любой программы на 10-15 минут</p><p><strong>Стоимость</strong> — 1100 руб.</p>', 1, 7000, NULL, '{}', NULL, 'goracie-apelsiny', 1489346121, 1),
(38, 1, 'Полина', '', 1, NULL, NULL, '{"height":"165","thigh":"90","breast":"1","working":"1"}', 'catalog/img7519-l-4c8fee1b22.jpg', 'polina', 1492021199, 1),
(39, 1, 'Селана', '', 1, NULL, NULL, '{"height":"165","thigh":"95","breast":"3","working":"1"}', 'catalog/photo2017-04-1719-30-53-215c4bf070.jpg', 'selana', 1492447118, 1),
(23, 1, 'Александра', '<p>Голубоглазая блондинка.</p>', 1, NULL, NULL, '{"height":"158","thigh":"90","breast":"2","working":"1"}', 'catalog/aleksandra-logo-6b58b367f0.jpg', 'aleksandra', 1489340408, 1),
(18, 1, 'Зара', '<p>Загадочная шатенка с карими глазами</p>', 1, NULL, NULL, '{"height":"167","thigh":"94","breast":"4","working":"1"}', 'catalog/zara-49af6c4e55.jpg', 'zara', 1491338154, 1),
(43, 1, 'Милена', '', 1, NULL, NULL, '{"height":"170","thigh":"101","breast":"2,5","working":"1"}', 'catalog/3-img6221-1-d6a07c59d8.jpg', 'milena', 1495201230, 1),
(25, 1, 'Лиза', '', 1, NULL, NULL, '{"height":"","thigh":"","breast":"","working":"1"}', 'catalog/57188aef222daliza1-97e5bc3f31.jpg', 'liza', 1490996610, 1),
(26, 1, 'Карина', '', 1, NULL, NULL, '{"height":"174","thigh":"96","breast":"3","working":"1"}', 'catalog/img7595-ef801e14cc.jpg', 'karina', 1491337002, 1),
(21, 1, 'Амина', '', 1, NULL, NULL, '{"height":"165","thigh":"90","breast":"2","working":"1"}', 'catalog/img7570-139e2b6288.jpg', 'amina', 1491338953, 1),
(22, 1, 'Жасмин', '', 1, NULL, NULL, '{"height":"165","thigh":"95","breast":"2","working":"1"}', 'catalog/img5554-70569358cf.jpg', 'zasmin', 1491337507, 1),
(27, 1, 'Сима', '', 1, NULL, NULL, '{"height":"165","thigh":"97","breast":"4","working":"1"}', 'catalog/photo-4-8257c5d2f6.jpg', 'sima', 1489340408, 1),
(28, 1, 'Ева', '', 1, NULL, NULL, '{"height":"164","thigh":"90","breast":"2","working":"1"}', 'catalog/photo-15-0922079995.jpg', 'eva', 1491338411, 1),
(29, 1, 'Милана', '', 1, NULL, NULL, '{"height":"173","thigh":"90","breast":"2","working":"1"}', 'catalog/milana-665460e9b0.jpg', 'milana', 1491339289, 1),
(30, 1, 'Доминика', '', 1, NULL, NULL, '{"height":"165","thigh":"95","breast":"3","working":"1"}', 'catalog/img5668-1bf1e909c0.jpg', 'dominika', 1491395899, 1),
(31, 1, 'Моника', '<p>Загадочная брюнетка.</p>', 1, NULL, NULL, '{"height":"163","thigh":"98","breast":"2","working":"1"}', 'catalog/4-img6342-2-d86fda297c.jpg', 'monika', 1491485849, 1),
(32, 1, 'Аня', '<p>Жгучая брюнетка</p>', 1, NULL, NULL, '{"height":"165","thigh":"88","breast":"2","working":"1"}', 'catalog/16-img5435-logo-2-596cdb032a-053a62da28.jpg', 'ana', 1491486157, 1),
(33, 1, 'Эмма', '<p>Зеленоглазая блондинка</p>', 1, NULL, NULL, '{"height":"170","thigh":"90","breast":"2","working":"1"}', 'catalog/12-img6506-e13361cd07.jpg', 'emma', 1491487555, 1),
(44, 1, 'Виола', '', 1, NULL, NULL, '{"height":"","thigh":"","breast":"","working":"1"}', NULL, 'viola', 1495207188, 0),
(45, 1, 'Лана', '', 1, NULL, NULL, '{"height":"","thigh":"","breast":"","working":"1"}', NULL, 'lana', 1495207240, 0),
(36, 1, 'Олеся', '', 1, NULL, NULL, '{"height":"160","thigh":"90","breast":"3","working":"1"}', 'catalog/0g2a7017-l-058dd8b4ee.jpg', 'olesa-2', 1491917060, 1),
(37, 1, 'Лейла', '', 1, NULL, NULL, '{"height":"163","thigh":"90","breast":"2","working":"1"}', 'catalog/img7469-51562dc01c.jpg', 'lejla', 1492020713, 1),
(40, 2, 'Программа «Бизнес-экспресс»', '<p "="">Быстрая программа, которая действует в будние дни с 10:00 до 20:00 и включает в себя купания, <strong>боди-массаж</strong> и одно расслабление. Подходит для тех, кто очень торопится, но хочет расслабиться при помощи эротического массажа.\r\n</p><p><strong>Цена:</strong>\r\n	<br> 1 девушка — 2 500 руб.\r\n</p><p "=""><br> <strong>Длительность:</strong> <em>30 мин</em>\r\n</p><iframe src="https://www.youtube.com/embed/20_rcbMZ3Pk" allowfullscreen="" frameborder="0" width="100%" height="315">\r\n</iframe><iframe src="https://www.youtube.com/embed/XiqcIOQArBA" allowfullscreen="" frameborder="0" width="100%" height="315">\r\n</iframe>', 1, 2500, NULL, '{}', NULL, 'programma-biznes-ekspress', 1493913250, 1),
(41, 1, 'Яна', '', 1, NULL, NULL, '{"height":"165","thigh":"85","breast":"2","working":"1"}', 'catalog/img9781-323a44e885.jpg', 'ana-2', 1494935839, 1),
(42, 1, 'Катя', '', 1, NULL, NULL, '{"height":"165","thigh":"95","breast":"2","working":"1"}', 'catalog/img9816-7882607fbf.jpg', 'kata', 1494936415, 1),
(46, 1, 'Марго', '', 1, NULL, NULL, '{"height":"","thigh":"","breast":"","working":"0"}', NULL, 'margo', 1495207263, 1),
(47, 1, 'Камелия', '', 1, NULL, NULL, '{"height":"","thigh":"","breast":"","working":"0"}', NULL, 'kamelia', 1495207282, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_catalog_item_data`
--

DROP TABLE IF EXISTS `easyii_catalog_item_data`;
CREATE TABLE `easyii_catalog_item_data` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `value` varchar(1024) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_catalog_item_data`
--

INSERT INTO `easyii_catalog_item_data` (`id`, `item_id`, `name`, `value`) VALUES
(701, 1, 'height', '169'),
(748, 38, 'working', '1'),
(746, 38, 'thigh', '90'),
(207, 23, 'breast', '2'),
(206, 23, 'thigh', '90'),
(466, 18, 'thigh', '94'),
(467, 18, 'breast', '4'),
(465, 18, 'height', '167'),
(672, 21, 'working', '1'),
(704, 1, 'working', '1'),
(205, 23, 'height', '158'),
(703, 1, 'breast', '1'),
(747, 38, 'breast', '1'),
(208, 23, 'working', '1'),
(468, 18, 'working', '1'),
(702, 1, 'thigh', '95'),
(669, 21, 'height', '165'),
(670, 21, 'thigh', '90'),
(671, 21, 'breast', '2'),
(688, 22, 'working', '1'),
(686, 22, 'thigh', '95'),
(687, 22, 'breast', '2'),
(745, 38, 'height', '165'),
(685, 22, 'height', '165'),
(809, 43, 'height', '170'),
(810, 43, 'thigh', '101'),
(811, 43, 'breast', '2,5'),
(493, 25, 'height', ''),
(494, 25, 'thigh', ''),
(495, 25, 'breast', ''),
(496, 25, 'working', '1'),
(696, 26, 'working', '1'),
(694, 26, 'thigh', '96'),
(695, 26, 'breast', '3'),
(712, 27, 'working', '1'),
(710, 27, 'thigh', '97'),
(711, 27, 'breast', '4'),
(680, 28, 'working', '1'),
(678, 28, 'thigh', '90'),
(679, 28, 'breast', '2'),
(709, 27, 'height', '165'),
(812, 43, 'working', '1'),
(693, 26, 'height', '174'),
(652, 29, 'working', '1'),
(650, 29, 'thigh', '90'),
(651, 29, 'breast', '2'),
(644, 30, 'working', '1'),
(642, 30, 'thigh', '95'),
(643, 30, 'breast', '3'),
(557, 31, 'height', '163'),
(558, 31, 'thigh', '98'),
(559, 31, 'breast', '2'),
(601, 32, 'height', '165'),
(602, 32, 'thigh', '88'),
(603, 32, 'breast', '2'),
(560, 31, 'working', '1'),
(604, 32, 'working', '1'),
(592, 33, 'working', '1'),
(590, 33, 'thigh', '90'),
(591, 33, 'breast', '2'),
(589, 33, 'height', '170'),
(836, 44, 'working', '1'),
(834, 44, 'thigh', ''),
(835, 44, 'breast', ''),
(649, 29, 'height', '173'),
(729, 36, 'height', '160'),
(641, 30, 'height', '165'),
(732, 36, 'working', '1'),
(730, 36, 'thigh', '90'),
(731, 36, 'breast', '3'),
(677, 28, 'height', '164'),
(744, 37, 'working', '1'),
(742, 37, 'thigh', '90'),
(743, 37, 'breast', '2'),
(741, 37, 'height', '163'),
(768, 39, 'working', '1'),
(766, 39, 'thigh', '95'),
(767, 39, 'breast', '3'),
(765, 39, 'height', '165'),
(780, 41, 'working', '1'),
(778, 41, 'thigh', '85'),
(779, 41, 'breast', '2'),
(777, 41, 'height', '165'),
(800, 42, 'working', '1'),
(798, 42, 'thigh', '95'),
(799, 42, 'breast', '2'),
(797, 42, 'height', '165'),
(833, 44, 'height', ''),
(839, 45, 'breast', ''),
(838, 45, 'thigh', ''),
(837, 45, 'height', ''),
(821, 46, 'height', ''),
(822, 46, 'thigh', ''),
(823, 46, 'breast', ''),
(824, 46, 'working', '0'),
(831, 47, 'breast', ''),
(830, 47, 'thigh', ''),
(829, 47, 'height', ''),
(832, 47, 'working', '0'),
(840, 45, 'working', '1');

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_entity_categories`
--

DROP TABLE IF EXISTS `easyii_entity_categories`;
CREATE TABLE `easyii_entity_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `fields` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `cache` tinyint(1) NOT NULL DEFAULT '1',
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_entity_categories`
--

INSERT INTO `easyii_entity_categories` (`id`, `title`, `description`, `image_file`, `fields`, `slug`, `cache`, `tree`, `lft`, `rgt`, `depth`, `order_num`, `status`) VALUES
(2, 'Салоны', '', NULL, '[{"name":"phone","title":"\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d","type":"string","options":""},{"name":"address","title":"\\u0410\\u0434\\u0440\\u0435\\u0441","type":"address","options":""},{"name":"addresstext","title":"\\u0410\\u0434\\u0440\\u0435\\u0441 (\\u0442\\u0435\\u043a\\u0441\\u0442)","type":"string","options":""},{"name":"addresshort","title":"\\u0410\\u0434\\u0440\\u0435\\u0441 (\\u043a\\u043e\\u0440\\u043e\\u0442\\u043a\\u0438\\u0439)","type":"string","options":""},{"name":"yandex","title":"\\u042f\\u043d\\u0434\\u0435\\u043a\\u0441. ID \\u043a\\u0430\\u0440\\u0442\\u044b","type":"string","options":""}]', 'salony', 1, 2, 1, 2, 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_entity_items`
--

DROP TABLE IF EXISTS `easyii_entity_items`;
CREATE TABLE `easyii_entity_items` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `data` text NOT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_entity_items`
--

INSERT INTO `easyii_entity_items` (`id`, `category_id`, `title`, `data`, `order_num`, `status`) VALUES
(2, 2, 'Каприз Северный', '{"phone":"+7 (928) 7-666-999","address":"\\u0411\\u0430\\u0442\\u0443\\u043c\\u0441\\u043a\\u0438\\u0439 \\u043f\\u0435\\u0440., 71, \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443, \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432\\u0441\\u043a\\u0430\\u044f \\u043e\\u0431\\u043b., \\u0420\\u043e\\u0441\\u0441\\u0438\\u044f, 344068#ChIJG6P9Kx-440AR6XlEOTfLVyg#47.27181909999999#39.713095899999985","addresstext":"\\u043f\\u0435\\u0440. \\u0411\\u0430\\u0442\\u0443\\u043c\\u0441\\u043a\\u0438\\u0439, 71, \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443","addresshort":"\\u043f\\u0435\\u0440. \\u0411\\u0430\\u0442\\u0443\\u043c\\u0441\\u043a\\u0438\\u0439, 71","yandex":"d757aa186c2fceaa54435755ab2f3b95b1dc555c0b907f083371b71112e823e7"}', 3, 1),
(3, 2, 'Каприз Западный', '{"phone":"+7 (988) 666-999-5","address":"\\u0443\\u043b. \\u0412\\u0441\\u0435\\u0441\\u043e\\u044e\\u0437\\u043d\\u0430\\u044f, 37, \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443, \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432\\u0441\\u043a\\u0430\\u044f \\u043e\\u0431\\u043b., \\u0420\\u043e\\u0441\\u0441\\u0438\\u044f, 344033#ChIJVXfeixu_40ARfc29TEdV_zs#47.1929258#39.61895609999999","addresstext":"\\u0443\\u043b. \\u0412\\u0441\\u0435\\u0441\\u043e\\u044e\\u0437\\u043d\\u0430\\u044f, 37, \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443","addresshort":"\\u0443\\u043b. \\u0412\\u0441\\u0435\\u0441\\u043e\\u044e\\u0437\\u043d\\u0430\\u044f, 37","yandex":"cea76ec679a730a6c36fcaf390bd484b9bbb4735f396bc7676dd1eb437257211"}', 2, 1),
(4, 2, 'Эрмитаж Румс', '{"phone":"+7 (928) 279-36-39","address":"\\u041f\\u0440\\u0435\\u0434\\u0431\\u043e\\u0442\\u0430\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u0443\\u043b., 2, \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443, \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432\\u0441\\u043a\\u0430\\u044f \\u043e\\u0431\\u043b., \\u0420\\u043e\\u0441\\u0441\\u0438\\u044f, 344041#ChIJcwLEs72440ARCqBJiaR6f3I#47.23471500000001#39.64170160000003","addresstext":"\\u0443\\u043b. \\u041f\\u0440\\u0435\\u0434\\u0431\\u043e\\u0442\\u0430\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f, 2, \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443","addresshort":"\\u0443\\u043b. \\u041f\\u0440\\u0435\\u0434\\u0431\\u043e\\u0442\\u0430\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f, 2","yandex":"f544da077274b1e0fbb0aeb1a705131dbaa727b87f25eba6df4ec43f5cdcf5be"}', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_faq`
--

DROP TABLE IF EXISTS `easyii_faq`;
CREATE TABLE `easyii_faq` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_feedback`
--

DROP TABLE IF EXISTS `easyii_feedback`;
CREATE TABLE `easyii_feedback` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `text` text NOT NULL,
  `answer_subject` varchar(128) DEFAULT NULL,
  `answer_text` text,
  `time` int(11) DEFAULT '0',
  `ip` varchar(16) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_feedback`
--

INSERT INTO `easyii_feedback` (`id`, `name`, `email`, `phone`, `title`, `text`, `answer_subject`, `answer_text`, `time`, `ip`, `status`) VALUES
(13, 'Посетитель', '', '+7 (962) 750 03 99', '', '', NULL, NULL, 1491406125, '46.147.125.168', 0),
(2, 'Посетитель', '', '+7 (928) 117 07 63', '', '', NULL, NULL, 1490373530, '31.173.201.77', 0),
(12, 'Посетитель', '', '+7 (928) 146 57 97', '', '', NULL, NULL, 1491219783, '31.23.34.184', 0),
(11, 'Посетитель', '', '+7 (928) 286 56 51', '', '', NULL, NULL, 1491066511, '185.3.33.37', 0),
(10, 'Посетитель', '', '+7 (928) 286 56 51', '', '', NULL, NULL, 1491042891, '185.3.33.114', 1),
(6, 'Посетитель', '', '+7 (903) 473 14 60', '', '', NULL, NULL, 1490723090, '188.168.215.46', 0),
(7, 'Посетитель', '', '+7 (903) 473 14 60', '', '', NULL, NULL, 1490723543, '188.168.215.46', 0),
(8, 'Посетитель', '', '+7 (903) 473 14 60', '', '', NULL, NULL, 1490723713, '188.168.215.46', 0),
(9, 'Посетитель', '', '+7 (928) 226 96 00', '', '', NULL, NULL, 1490806547, '188.162.166.58', 0),
(14, 'Посетитель', '', '89043442646', '', '', NULL, NULL, 1491524164, '95.153.130.186', 0),
(15, 'Посетитель', '', '+7 (903) 473 14 60', '', '', NULL, NULL, 1491590762, '188.168.215.46', 0),
(16, 'Посетитель', '', '8-928-1534123', '', '', NULL, NULL, 1491601281, '185.3.33.31', 0),
(17, 'Посетитель', '', '8-928-1534123', '', '', NULL, NULL, 1491601284, '185.3.33.31', 0),
(18, 'Посетитель', '', '8-928-1534123', '', '', NULL, NULL, 1491601287, '185.3.33.31', 0),
(19, 'Посетитель', '', '8-928-1534123', '', '', NULL, NULL, 1491601290, '185.3.33.31', 0),
(20, 'Посетитель', '', '8-928-1534123', '', '', NULL, NULL, 1491601293, '185.3.33.31', 0),
(21, 'Посетитель', '', '89034023015', '', '', NULL, NULL, 1491693194, '188.170.198.171', 0),
(22, 'Посетитель', '', '+7 (905) 426 31 93', '', '', NULL, NULL, 1491723464, '85.26.183.194', 0),
(23, 'Посетитель', '', '+7 (950) 851 86 80', '', '', NULL, NULL, 1491746469, '77.66.150.114', 0),
(24, 'Посетитель', '', '+7 (950) 851 86 80', '', '', NULL, NULL, 1491746482, '77.66.150.114', 0),
(25, 'Посетитель', '', '89635861545', '', '', NULL, NULL, 1491753390, '217.118.81.220', 0),
(26, 'Посетитель', '', '+7 (968) 286 19 29', '', '', NULL, NULL, 1491779892, '217.118.81.19', 0),
(27, 'Посетитель', '', '89515013551', '', '', NULL, NULL, 1491827178, '95.153.131.140', 0),
(28, 'Посетитель', '', '89515013551', '', '', NULL, NULL, 1491827180, '95.153.131.140', 1),
(29, 'Посетитель', '', '89515013551', '', '', NULL, NULL, 1491827183, '95.153.131.140', 0),
(30, 'Посетитель', '', '89885866468', '', '', NULL, NULL, 1492117479, '37.144.32.78', 0),
(31, 'Посетитель', '', '89885866468', '', '', NULL, NULL, 1492117486, '37.144.32.78', 0),
(32, 'Посетитель', '', '+7 (951) 492 14 31', '', '', NULL, NULL, 1492199631, '31.23.184.48', 0),
(33, 'Посетитель', '', '', '', '', NULL, NULL, 1492714765, '217.118.81.20', 0),
(34, 'Посетитель', '', '', '', '', NULL, NULL, 1492731231, '217.118.81.26', 0),
(35, 'Посетитель', '', '', '', '', NULL, NULL, 1492770821, '212.192.204.44', 0),
(36, 'Посетитель', '', '+7 (777) 777 77 77', '', '', NULL, NULL, 1492771447, '212.192.204.44', 0),
(37, 'Посетитель', '', '89626439999', '', '', NULL, NULL, 1492821315, '217.118.81.245', 0),
(38, 'Посетитель', '', '89626439999', '', '', NULL, NULL, 1492823353, '217.118.81.245', 0),
(39, 'Посетитель', '', '89897190255', '', '', NULL, NULL, 1493025920, '87.117.12.44', 0),
(40, 'Посетитель', '', '89897190255', '', '', NULL, NULL, 1493025937, '87.117.12.44', 0),
(41, 'Посетитель', '', '+7 (951) 492 55 95', '', '', NULL, NULL, 1493115198, '188.170.195.176', 1),
(42, 'Посетитель', '', '+7 (903) 473 14 60', '', '', NULL, NULL, 1493122542, '212.192.204.44', 0),
(43, 'Посетитель', '', '+7 (923) 524 95 95', '', '', NULL, NULL, 1493159305, '85.26.183.240', 0),
(44, 'Посетитель', '', '+79287771012', '', '', NULL, NULL, 1493217152, '212.192.204.44', 0),
(45, 'Посетитель', '', '+7 (918) 500 80 23', '', '', NULL, NULL, 1493385281, '87.117.16.101', 0),
(46, 'Посетитель', '', '+7 (950) 956 81 15', '', '', NULL, NULL, 1493837969, '188.232.232.37', 0),
(47, 'Посетитель', '', '+7 (938) 304 81 43', '', '', NULL, NULL, 1493960545, '176.96.191.182', 0),
(48, 'Посетитель', '', '+7 (961) 432 71 45', '', '', NULL, NULL, 1494365084, '217.118.81.219', 0),
(49, 'Посетитель', '', '+7 (908) 500 49 49', '', '', NULL, NULL, 1494400032, '213.27.25.35', 0),
(50, 'Посетитель', '', '+7 (938) 112 61 20', '', '', NULL, NULL, 1494417062, '213.138.86.15', 0),
(51, 'Посетитель', '', '+7 (938) 112 61 06', '', '', NULL, NULL, 1494417084, '213.138.86.15', 0),
(52, 'Посетитель', '', '+7 (938) 112 61 06', '', '', NULL, NULL, 1494417170, '213.138.86.15', 0),
(53, 'Посетитель', '', '+7 (928) 777 10 12', '', '', NULL, NULL, 1494430179, '212.192.204.44', 0),
(54, 'Посетитель', '', '+7 (961) 581 71 96', '', '', NULL, NULL, 1494437779, '176.59.69.5', 0),
(55, 'Посетитель', '', '+7 (909) 441 04 10', '', '', NULL, NULL, 1494447349, '217.118.81.222', 0),
(56, 'Посетитель', '', '+7 (968) 711 75 45', '', '', NULL, NULL, 1494550658, '188.162.166.197', 0),
(57, 'Посетитель', '', '+7 (937) 777 10 17', '', '', NULL, NULL, 1494555834, '85.26.165.204', 0),
(58, 'Посетитель', '', '+7 (925) 700 69 99', '', '', NULL, NULL, 1494629208, '31.173.85.147', 0),
(59, 'Посетитель', '', '+7 (799) 969 26 43', '', '', NULL, NULL, 1494753654, '188.162.167.137', 0),
(60, 'Посетитель', '', '+7 (908) 185 83 71', '', '', NULL, NULL, 1495008043, '185.3.32.209', 0),
(61, 'Посетитель', '', '+7 (951) 523 48 78', '', '', NULL, NULL, 1495518733, '188.162.167.193', 0),
(62, 'Посетитель', '', '+7 (904) 342 25 20', '', '', NULL, NULL, 1495622121, '46.147.124.200', 0),
(63, 'Посетитель', '', '+7 (908) 193 44 29', '', '', NULL, NULL, 1495718040, '77.95.91.254', 0),
(64, 'Посетитель', '', '+7 (911) 111 11 86', '', '', NULL, NULL, 1495810166, '95.153.128.236', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_files`
--

DROP TABLE IF EXISTS `easyii_files`;
CREATE TABLE `easyii_files` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `file` varchar(255) NOT NULL,
  `size` int(11) DEFAULT '0',
  `slug` varchar(128) DEFAULT NULL,
  `downloads` int(11) DEFAULT '0',
  `time` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_gallery_categories`
--

DROP TABLE IF EXISTS `easyii_gallery_categories`;
CREATE TABLE `easyii_gallery_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_gallery_categories`
--

INSERT INTO `easyii_gallery_categories` (`id`, `title`, `description`, `image_file`, `slug`, `tree`, `lft`, `rgt`, `depth`, `order_num`, `status`) VALUES
(1, 'Интерьеры', '', NULL, 'interery', 1, 1, 2, 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_guestbook`
--

DROP TABLE IF EXISTS `easyii_guestbook`;
CREATE TABLE `easyii_guestbook` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `text` text NOT NULL,
  `answer` text,
  `email` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `ip` varchar(16) DEFAULT NULL,
  `new` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_loginform`
--

DROP TABLE IF EXISTS `easyii_loginform`;
CREATE TABLE `easyii_loginform` (
  `id` int(11) NOT NULL,
  `username` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `user_agent` varchar(1024) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `success` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_loginform`
--

INSERT INTO `easyii_loginform` (`id`, `username`, `password`, `ip`, `user_agent`, `time`, `success`) VALUES
(1, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0', 1486491287, 1),
(2, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0', 1486918666, 1),
(3, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0', 1487271624, 1),
(4, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0', 1487698129, 1),
(5, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0', 1489241598, 1),
(6, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0', 1489329600, 1),
(7, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0', 1489482950, 1),
(8, 'root', '******', '188.168.215.29', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', 1489952871, 1),
(9, 'root', '******', '188.168.215.29', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', 1490121401, 1),
(10, 'root', '******', '188.168.215.29', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', 1490216983, 1),
(11, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0', 1490360967, 1),
(12, 'root', '******', '188.168.215.29', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', 1490391000, 1),
(13, 'root', '******', '188.168.215.46', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', 1490720234, 1),
(14, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0', 1490779287, 1),
(15, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1490973326, 1),
(16, 'root', '******', '178.155.5.215', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 1490996146, 1),
(17, 'root', '******', '178.155.5.215', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/10.0 Mobile/14D27 Safari/602.1', 1491042993, 1),
(18, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1491219751, 1),
(19, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 1491235459, 1),
(20, 'root', '******', '178.155.5.31', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 1491336982, 1),
(21, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 1491394512, 1),
(22, 'root', 'XySolQJo', '85.26.209.152', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491485666, 0),
(23, 'root', 'XySolQJo', '85.26.209.152', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491485687, 0),
(24, 'root', '******', '85.26.209.152', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491485728, 1),
(25, 'root', '******', '188.168.215.46', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', 1491506487, 1),
(26, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1491551523, 1),
(27, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491551800, 1),
(28, 'root', '******', '85.26.209.152', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491577206, 1),
(29, ' root', 'XySoIQJo', '85.26.209.152', 'Mozilla/5.0 (Linux; Android 4.4.2; GT-I9192 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.141 Mobile Safari/537.36', 1491827479, 0),
(30, ' root', 'XySoIQJo', '85.26.209.152', 'Mozilla/5.0 (Linux; Android 4.4.2; GT-I9192 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.141 Mobile Safari/537.36', 1491827503, 0),
(31, ' root', 'XySolQGo', '85.26.209.152', 'Mozilla/5.0 (Linux; Android 4.4.2; GT-I9192 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.141 Mobile Safari/537.36', 1491827523, 0),
(32, 'root', '******', '85.26.209.152', 'Mozilla/5.0 (Linux; Android 4.4.2; GT-I9192 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.141 Mobile Safari/537.36', 1491827567, 1),
(33, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1491828045, 1),
(34, ' root ', ' XySoIQJo', '188.170.93.158', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491846103, 0),
(35, ' root ', 'XySoIQJo', '188.170.93.158', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491846242, 0),
(36, ' Root ', 'XySoIQJo', '188.170.93.158', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491846253, 0),
(37, ' root ', 'XySoIQJo', '188.170.93.158', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491846260, 0),
(38, 'root ', 'XySoIQJo', '188.170.93.158', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491846269, 0),
(39, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491846450, 1),
(40, 'root ', 'XySoIQJo', '188.170.93.158', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491846494, 0),
(41, 'root ', 'XySoIQgo', '188.170.93.158', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491847126, 0),
(42, 'root ', 'XySoIQJo', '188.170.93.158', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491847200, 0),
(43, 'root ', 'XySoIQJo', '188.170.93.158', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491847271, 0),
(44, 'root', '******', '188.170.93.158', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1491847595, 1),
(45, 'root', ' XySoIQJo', '188.170.93.158', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393', 1491916959, 0),
(46, 'root', '******', '188.170.93.158', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393', 1491917039, 1),
(47, 'root', '******', '188.168.215.39', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', 1491942139, 1),
(48, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1492070809, 1),
(49, 'root', '******', '188.168.215.39', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', 1492245555, 1),
(50, 'root', '******', '188.170.93.158', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393', 1492447092, 1),
(51, 'root', '******', '188.168.215.39', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', 1492449779, 1),
(52, 'root', '******', '188.168.215.39', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', 1492547070, 1),
(53, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1492616737, 1),
(54, 'root', '******', '188.168.215.39', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0', 1492722377, 1),
(55, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', 1492770879, 1),
(56, 'root', '******', '195.151.27.189', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1', 1493121914, 1),
(57, 'root', '******', '188.168.215.20', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0', 1493749950, 1),
(58, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1493913206, 1),
(59, 'root', '******', '188.168.215.9', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0', 1494534849, 1),
(60, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1494916754, 1),
(61, 'root', '******', '188.170.93.158', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393', 1494935526, 1),
(62, 'root', '******', '188.168.215.49', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0', 1494956605, 1),
(63, 'root', '******', '195.151.27.189', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1495201178, 1),
(64, 'root', '******', '188.168.215.49', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0', 1495351224, 1),
(65, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1495526027, 1),
(66, 'root', '******', '188.168.215.24', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0', 1495738198, 1),
(67, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1495793113, 1),
(68, 'root', '******', '188.168.215.24', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0', 1496077297, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_menu`
--

DROP TABLE IF EXISTS `easyii_menu`;
CREATE TABLE `easyii_menu` (
  `menu_id` int(11) NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `items` text,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_menu`
--

INSERT INTO `easyii_menu` (`menu_id`, `slug`, `title`, `items`, `status`) VALUES
(1, 'main', 'Главное меню', '[\n    {\n        "label": "Девушки",\n        "url": "/#girls"\n    },\n    {\n        "label": "Акции и цены",\n        "url": "/#actions"\n    },\n    {\n        "label": "Интерьеры",\n        "url": "/#interior"\n    },\n    {\n        "label": "Работа",\n        "url": "/rabota"\n    },\n    {\n        "label": "Адреса салонов",\n        "url": "/#map"\n    }\n]', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_migration`
--

DROP TABLE IF EXISTS `easyii_migration`;
CREATE TABLE `easyii_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `easyii_migration`
--

INSERT INTO `easyii_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1486491285),
('m000000_000000_install', 1486491286),
('m000009_100000_update', 1486491286),
('m000009_200000_update', 1486491286),
('m000009_200003_module_menu', 1486491286),
('m000009_200004_update', 1486491286);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_modules`
--

DROP TABLE IF EXISTS `easyii_modules`;
CREATE TABLE `easyii_modules` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `class` varchar(128) NOT NULL,
  `title` varchar(128) NOT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `settings` text,
  `notice` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_modules`
--

INSERT INTO `easyii_modules` (`id`, `name`, `class`, `title`, `icon`, `settings`, `notice`, `order_num`, `status`) VALUES
(1, 'entity', 'yii\\easyii\\modules\\entity\\EntityModule', 'Объекты', 'asterisk', '{"categoryThumb":true,"categorySlugImmutable":false,"categoryDescription":true,"itemsInFolder":false}', 57, 95, 1),
(2, 'article', 'yii\\easyii\\modules\\article\\ArticleModule', 'Статьи', 'pencil', '{"categoryThumb":true,"categorySlugImmutable":false,"categoryDescription":true,"articleThumb":true,"enablePhotos":true,"enableTags":true,"enableShort":true,"shortMaxLength":"255","itemsInFolder":true,"itemSlugImmutable":false}', 0, 60, 0),
(3, 'carousel', 'yii\\easyii\\modules\\carousel\\CarouselModule', 'Акции', 'picture', '{"enableTitle":true,"enableText":true}', 0, 40, 1),
(4, 'catalog', 'yii\\easyii\\modules\\catalog\\CatalogModule', 'Каталог', 'list-alt', '{"categoryThumb":false,"categorySlugImmutable":false,"categoryDescription":false,"itemsInFolder":false,"itemThumb":true,"itemPhotos":true,"itemDescription":true,"itemSlugImmutable":false}', 0, 100, 1),
(5, 'faq', 'yii\\easyii\\modules\\faq\\FaqModule', 'Вопросы и ответы', 'question-sign', '{"questionHtmlEditor":true,"answerHtmlEditor":true,"enableTags":true}', 0, 45, 0),
(6, 'feedback', 'yii\\easyii\\modules\\feedback\\FeedbackModule', 'Обратная связь', 'earphone', '{"mailAdminOnNewFeedback":true,"subjectOnNewFeedback":"\\u041d\\u043e\\u0432\\u0430\\u044f \\u0437\\u0430\\u044f\\u0432\\u043a\\u0430 \\u0441 \\u0441\\u0430\\u0439\\u0442\\u0430","templateOnNewFeedback":"@app\\/mail\\/new_feedback","answerTemplate":"@easyii\\/modules\\/feedback\\/mail\\/en\\/answer","answerSubject":"Answer on your feedback message","answerHeader":"Hello,","answerFooter":"Best regards.","telegramAdminOnNewFeedback":true,"telegramTemplateOnNewFeedback":"@app\\/telegram\\/new_feedback","enableTitle":false,"enableEmail":true,"enablePhone":true,"enableText":true,"enableCaptcha":false}', 34, 51, 1),
(7, 'file', 'yii\\easyii\\modules\\file\\FileModule', 'Файлы', 'floppy-disk', '{"slugImmutable":false}', 0, 30, 0),
(8, 'gallery', 'yii\\easyii\\modules\\gallery\\GalleryModule', 'Фотогалерея', 'camera', '{"categoryThumb":true,"itemsInFolder":false,"categoryTags":true,"categorySlugImmutable":false,"categoryDescription":true}', 0, 90, 1),
(9, 'guestbook', 'yii\\easyii\\modules\\guestbook\\GuestbookModule', 'Гостевая книга', 'book', '{"enableTitle":false,"enableEmail":true,"preModerate":true,"enableCaptcha":false,"mailAdminOnNewPost":true,"subjectOnNewPost":"New message in the guestbook.","templateOnNewPost":"@easyii\\/modules\\/guestbook\\/mail\\/en\\/new_post","frontendGuestbookRoute":"\\/guestbook","subjectNotifyUser":"Your post in the guestbook answered","templateNotifyUser":"@easyii\\/modules\\/guestbook\\/mail\\/en\\/notify_user"}', 0, 80, 1),
(10, 'menu', 'yii\\easyii\\modules\\menu\\MenuModule', 'Меню', 'menu-hamburger', '{"slugImmutable":false}', 0, 50, 1),
(11, 'news', 'yii\\easyii\\modules\\news\\NewsModule', 'Новости', 'bullhorn', '{"enableThumb":true,"enablePhotos":true,"enableShort":true,"shortMaxLength":256,"enableTags":true,"slugImmutable":false}', 0, 70, 0),
(12, 'page', 'yii\\easyii\\modules\\page\\PageModule', 'Страницы', 'file', '{"slugImmutable":true,"defaultFields":"[]"}', 0, 65, 1),
(13, 'shopcart', 'yii\\easyii\\modules\\shopcart\\ShopcartModule', 'Заказы', 'shopping-cart', '{"mailAdminOnNewOrder":true,"subjectOnNewOrder":"New order","templateOnNewOrder":"@easyii\\/modules\\/shopcart\\/mail\\/en\\/new_order","subjectNotifyUser":"Your order status changed","templateNotifyUser":"@easyii\\/modules\\/shopcart\\/mail\\/en\\/notify_user","frontendShopcartRoute":"\\/shopcart\\/order","enablePhone":true,"enableEmail":true}', 0, 120, 0),
(14, 'subscribe', 'yii\\easyii\\modules\\subscribe\\SubscribeModule', 'E-mail рассылка', 'envelope', '[]', 0, 10, 0),
(15, 'text', 'yii\\easyii\\modules\\text\\TextModule', 'Текстовые блоки', 'font', '[]', 0, 20, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_news`
--

DROP TABLE IF EXISTS `easyii_news`;
CREATE TABLE `easyii_news` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `image_file` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_pages`
--

DROP TABLE IF EXISTS `easyii_pages`;
CREATE TABLE `easyii_pages` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `show_in_menu` tinyint(1) DEFAULT '0',
  `fields` text,
  `data` text,
  `tree` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0',
  `depth` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_pages`
--

INSERT INTO `easyii_pages` (`id`, `title`, `text`, `slug`, `show_in_menu`, `fields`, `data`, `tree`, `lft`, `rgt`, `depth`, `order_num`, `status`) VALUES
(1, 'Главная', '', 'index', 0, '{}', '{}', 1, 1, 2, 0, 1, 1),
(2, 'Работа', '', 'rabota', 0, '{}', '{}', 2, 1, 2, 0, 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_photos`
--

DROP TABLE IF EXISTS `easyii_photos`;
CREATE TABLE `easyii_photos` (
  `id` int(11) NOT NULL,
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `image_file` varchar(128) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_photos`
--

INSERT INTO `easyii_photos` (`id`, `class`, `item_id`, `image_file`, `description`, `order_num`) VALUES
(3, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/9-b80b8df2dc.jpg', '', 3),
(4, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/17-4a47d2983c.jpg', '', 5),
(5, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/14-3cf2559725.jpg', '', 6),
(6, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/16-05204fdf0a.jpg', '', 22),
(42, 'yii\\easyii\\modules\\catalog\\models\\Item', 23, 'catalog/aleksandra2-a0ffd3dc05.jpg', '', 40),
(43, 'yii\\easyii\\modules\\catalog\\models\\Item', 23, 'catalog/aleksandra1-ebf1cd5ba6.jpg', '', 41),
(96, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/photo2017-04-1719-30-43-54fc67cbb5.jpg', '', 94),
(13, 'yii\\easyii\\modules\\catalog\\models\\Item', 18, 'catalog/darina-24582323ab18be-3eba501b34.jpg', '', 13),
(16, 'yii\\easyii\\modules\\catalog\\models\\Item', 21, 'catalog/1-d4853ab80c.jpg', '', 16),
(18, 'yii\\easyii\\modules\\catalog\\models\\Item', 21, 'catalog/2-a77fb8e50b.jpg', '', 18),
(19, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/jasmin2-c18a8525eb.jpg', '', 19),
(20, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/jasmin3-780929a318.jpg', '', 20),
(21, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/jasmin1-cecc33c5cb.jpg', '', 21),
(98, 'yii\\easyii\\modules\\catalog\\models\\Item', 41, 'catalog/img9773-282532e4d4.jpg', '', 95),
(26, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/bilardnaa-d0599e2e9d.jpg', '', 29),
(28, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/spazona2-7d9f28141e.jpg', '', 30),
(100, 'yii\\easyii\\modules\\catalog\\models\\Item', 41, 'catalog/img9965-f7d9ec065b.jpg', '', 97),
(95, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/photo2017-04-1719-29-23-5e032a1a16.jpg', '', 93),
(105, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/image2ashx-0f34b949d8.jpg', '', 101),
(102, 'yii\\easyii\\modules\\catalog\\models\\Item', 42, 'catalog/img9837-450ad818ec.jpg', '', 99),
(103, 'yii\\easyii\\modules\\catalog\\models\\Item', 42, 'catalog/img9935-58fedf8566.jpg', '', 100),
(104, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/imageashx-5910045b15.jpg', '', 103),
(36, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/photo2017-03-1611-54-01-7f58141c15.jpg', '', 38),
(108, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/1-img6166-2-4fc7a6e339.jpg', '', 105),
(39, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/photo2017-03-1611-54-05-6e1a9464f1.jpg', '', 4),
(40, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/zapadnyj-462e4031f9.jpg', '', 23),
(41, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/interer15-9da174f9e0.jpg', '', 24),
(60, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/milana1m-d9c5bf76a1.jpg', '', 59),
(48, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/57188aee3e41eliza3-59c3d66f15.jpg', '', 46),
(49, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/57188aed7eb6cliza4-a346ffa560.jpg', '', 47),
(51, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/57188af02cadfliza2-e8e62fc192.jpg', '', 48),
(52, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/kari1-2bf3899548.jpg', '', 50),
(53, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/kari3-d7b929d26e.jpg', '', 51),
(55, 'yii\\easyii\\modules\\catalog\\models\\Item', 27, 'catalog/photo-6-bf35030e4e.jpg', '', 53),
(57, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/eva1-724de08c0c.jpg', '', 55),
(59, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/eva3-7afd587d2a.jpg', '', 57),
(61, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/milana2m-c0563a4131.jpg', '', 60),
(62, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/mmg1078-0a63712c0d.jpg', '', 58),
(64, 'yii\\easyii\\modules\\catalog\\models\\Item', 30, 'catalog/img5771-d4475907ac.jpg', '', 62),
(65, 'yii\\easyii\\modules\\catalog\\models\\Item', 31, 'catalog/6-img6301-2-3e3cef4bc0.jpg', '', 63),
(66, 'yii\\easyii\\modules\\catalog\\models\\Item', 32, 'catalog/15-img5351-2-891adcceb7.jpg', '', 64),
(70, 'yii\\easyii\\modules\\catalog\\models\\Item', 33, 'catalog/13-img6491-9e60ad0e20.jpg', '', 68),
(109, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/2-img6195-f3138c0756.jpg', '', 106),
(74, 'yii\\easyii\\modules\\catalog\\models\\Item', 32, 'catalog/14-img5376-2-d376dfb901.jpg', '', 72),
(77, 'yii\\easyii\\modules\\catalog\\models\\Item', 21, 'catalog/img5495-d58e635150.jpg', '', 75),
(80, 'yii\\easyii\\modules\\catalog\\models\\Item', 30, 'catalog/20-img9658-dee60ff85c.jpg', '', 78),
(81, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/img5531-5912957a62.jpg', '', 79),
(85, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/img7617-18a48c9c54.jpg', '', 83),
(86, 'yii\\easyii\\modules\\catalog\\models\\Item', 1, 'catalog/0g2a6691-l-fa7d913d31.jpg', '', 84),
(106, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/image3ashx-b99e240888.jpg', '', 102),
(88, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/0g2a6981-l-1-4a21bc03be.jpg', '', 86),
(89, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/0g2a7055-l-e9066c2e95.jpg', '', 87),
(91, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/lejla-3-af56d4a3cb.jpg', '', 89),
(92, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/lejla-1-4e20c56310.jpg', '', 90),
(94, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/0g2a7410-24c3ece36b.jpg', '', 92);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_seotext`
--

DROP TABLE IF EXISTS `easyii_seotext`;
CREATE TABLE `easyii_seotext` (
  `id` int(11) NOT NULL,
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_seotext`
--

INSERT INTO `easyii_seotext` (`id`, `class`, `item_id`, `h1`, `title`, `keywords`, `description`) VALUES
(1, 'yii\\easyii\\modules\\page\\models\\Page', 1, 'Сеть салонов эротической VIP релаксации', 'Сеть салонов эротической VIP релаксации', '', ''),
(2, 'yii\\easyii\\modules\\page\\models\\Page', 2, 'Работа массажисткой в VIP-комплексе «Эрмитаж»', 'Высокооплачиваемая работа для девушек в Ростове-на-Дону без интима', '', 'Работа массажисткой в VIP-комплексах Эрмитаж и Каприз - обучение и жилье бесплатно. Зарабатывай до 300 тыс/месяц');

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_settings`
--

DROP TABLE IF EXISTS `easyii_settings`;
CREATE TABLE `easyii_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `title` varchar(128) NOT NULL,
  `value` varchar(1024) DEFAULT NULL,
  `visibility` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_settings`
--

INSERT INTO `easyii_settings` (`id`, `name`, `title`, `value`, `visibility`) VALUES
(1, 'easyii_version', 'EasyiiCMS version', '0.91', 0),
(2, 'image_max_width', 'Максимальная ширина загружаемых изображений, которые автоматически не сжимаются', '1900', 2),
(3, 'redactor_plugins', 'Список плагинов редактора Redactor через запятую', 'imagemanager, filemanager, table, fullscreen', 1),
(4, 'ga_service_email', 'E-mail сервис аккаунта Google Analytics', '', 1),
(5, 'ga_profile_id', 'Номер профиля Google Analytics', '', 1),
(6, 'ga_p12_file', 'Путь к файлу ключей p12 сервис аккаунта Google Analytics', '', 1),
(7, 'gm_api_key', 'Google Maps API ключ', 'AIzaSyD3TNxUzBswDgaODRYXWsHdWgfwe-i3Vm0', 1),
(8, 'recaptcha_key', 'ReCaptcha key', '', 1),
(9, 'password_salt', 'Password salt', 'h8ldUFATkxtGo6LLk7cwuSLzWLK3aIBw', 0),
(10, 'root_auth_key', 'Root authorization key', 'NdUKjibYKj8nR7J2X6e-0p8VA5ua4Dw4', 0),
(11, 'root_password', 'Пароль разработчика', 'ed097d72248085a74d2a629eaa786369bfe5f085', 1),
(12, 'auth_time', 'Время авторизации', '86400', 1),
(13, 'robot_email', 'E-mail рассыльщика', 'goldenkapriz@yandex.ru', 1),
(14, 'admin_email', 'E-mail администратора', 'goldenkapriz@yandex.ru', 2),
(15, 'recaptcha_secret', 'ReCaptcha secret', '', 1),
(16, 'toolbar_position', 'Позиция панели на сайте ("top" or "bottom" or "hide")', 'hide', 1),
(17, 'name', 'Название сайта', 'Эротический салон VIP релаксации', 2),
(20, 'telegram_bot_token', 'Токен для бота телеграмм', '330188338:AAHwVl1En86_x-0qmKOqpN_jqgJKZeGOU04', 2),
(21, 'telegram_chat_id', 'ID чата в телеграмме для уведомлений', '-1001117473219', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_shopcart_goods`
--

DROP TABLE IF EXISTS `easyii_shopcart_goods`;
CREATE TABLE `easyii_shopcart_goods` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `options` varchar(255) DEFAULT NULL,
  `price` float DEFAULT '0',
  `discount` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_shopcart_orders`
--

DROP TABLE IF EXISTS `easyii_shopcart_orders`;
CREATE TABLE `easyii_shopcart_orders` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `address` varchar(1024) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `comment` varchar(1024) DEFAULT NULL,
  `remark` varchar(1024) DEFAULT NULL,
  `access_token` varchar(32) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `new` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_subscribe_history`
--

DROP TABLE IF EXISTS `easyii_subscribe_history`;
CREATE TABLE `easyii_subscribe_history` (
  `id` int(11) NOT NULL,
  `subject` varchar(128) NOT NULL,
  `body` text,
  `sent` int(11) DEFAULT '0',
  `time` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_subscribe_subscribers`
--

DROP TABLE IF EXISTS `easyii_subscribe_subscribers`;
CREATE TABLE `easyii_subscribe_subscribers` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `time` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_tags`
--

DROP TABLE IF EXISTS `easyii_tags`;
CREATE TABLE `easyii_tags` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `frequency` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_tags_assign`
--

DROP TABLE IF EXISTS `easyii_tags_assign`;
CREATE TABLE `easyii_tags_assign` (
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_texts`
--

DROP TABLE IF EXISTS `easyii_texts`;
CREATE TABLE `easyii_texts` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `easyii_admins`
--
ALTER TABLE `easyii_admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `access_token` (`access_token`);

--
-- Индексы таблицы `easyii_article_categories`
--
ALTER TABLE `easyii_article_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_article_items`
--
ALTER TABLE `easyii_article_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_carousel`
--
ALTER TABLE `easyii_carousel`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_catalog_categories`
--
ALTER TABLE `easyii_catalog_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_catalog_items`
--
ALTER TABLE `easyii_catalog_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_catalog_item_data`
--
ALTER TABLE `easyii_catalog_item_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_id_name` (`item_id`,`name`),
  ADD KEY `value` (`value`(300));

--
-- Индексы таблицы `easyii_entity_categories`
--
ALTER TABLE `easyii_entity_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_entity_items`
--
ALTER TABLE `easyii_entity_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_faq`
--
ALTER TABLE `easyii_faq`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_feedback`
--
ALTER TABLE `easyii_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_files`
--
ALTER TABLE `easyii_files`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_gallery_categories`
--
ALTER TABLE `easyii_gallery_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_guestbook`
--
ALTER TABLE `easyii_guestbook`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_loginform`
--
ALTER TABLE `easyii_loginform`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_menu`
--
ALTER TABLE `easyii_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Индексы таблицы `easyii_migration`
--
ALTER TABLE `easyii_migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `easyii_modules`
--
ALTER TABLE `easyii_modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `easyii_news`
--
ALTER TABLE `easyii_news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_pages`
--
ALTER TABLE `easyii_pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_photos`
--
ALTER TABLE `easyii_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_item` (`class`,`item_id`);

--
-- Индексы таблицы `easyii_seotext`
--
ALTER TABLE `easyii_seotext`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `model_item` (`class`,`item_id`);

--
-- Индексы таблицы `easyii_settings`
--
ALTER TABLE `easyii_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `easyii_shopcart_goods`
--
ALTER TABLE `easyii_shopcart_goods`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_shopcart_orders`
--
ALTER TABLE `easyii_shopcart_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_subscribe_history`
--
ALTER TABLE `easyii_subscribe_history`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_subscribe_subscribers`
--
ALTER TABLE `easyii_subscribe_subscribers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Индексы таблицы `easyii_tags`
--
ALTER TABLE `easyii_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `easyii_tags_assign`
--
ALTER TABLE `easyii_tags_assign`
  ADD KEY `class` (`class`),
  ADD KEY `item_tag` (`item_id`,`tag_id`);

--
-- Индексы таблицы `easyii_texts`
--
ALTER TABLE `easyii_texts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `easyii_admins`
--
ALTER TABLE `easyii_admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_article_categories`
--
ALTER TABLE `easyii_article_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_article_items`
--
ALTER TABLE `easyii_article_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_carousel`
--
ALTER TABLE `easyii_carousel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `easyii_catalog_categories`
--
ALTER TABLE `easyii_catalog_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `easyii_catalog_items`
--
ALTER TABLE `easyii_catalog_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT для таблицы `easyii_catalog_item_data`
--
ALTER TABLE `easyii_catalog_item_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=841;
--
-- AUTO_INCREMENT для таблицы `easyii_entity_categories`
--
ALTER TABLE `easyii_entity_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `easyii_entity_items`
--
ALTER TABLE `easyii_entity_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `easyii_faq`
--
ALTER TABLE `easyii_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_feedback`
--
ALTER TABLE `easyii_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT для таблицы `easyii_files`
--
ALTER TABLE `easyii_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_gallery_categories`
--
ALTER TABLE `easyii_gallery_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `easyii_guestbook`
--
ALTER TABLE `easyii_guestbook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_loginform`
--
ALTER TABLE `easyii_loginform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT для таблицы `easyii_menu`
--
ALTER TABLE `easyii_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `easyii_modules`
--
ALTER TABLE `easyii_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `easyii_news`
--
ALTER TABLE `easyii_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_pages`
--
ALTER TABLE `easyii_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `easyii_photos`
--
ALTER TABLE `easyii_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT для таблицы `easyii_seotext`
--
ALTER TABLE `easyii_seotext`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `easyii_settings`
--
ALTER TABLE `easyii_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `easyii_shopcart_goods`
--
ALTER TABLE `easyii_shopcart_goods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_shopcart_orders`
--
ALTER TABLE `easyii_shopcart_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_subscribe_history`
--
ALTER TABLE `easyii_subscribe_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_subscribe_subscribers`
--
ALTER TABLE `easyii_subscribe_subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_tags`
--
ALTER TABLE `easyii_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_texts`
--
ALTER TABLE `easyii_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
