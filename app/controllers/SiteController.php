<?php

namespace app\controllers;

use app\models\GuestbookForm;
use yii\easyii\modules\carousel\api\Carousel;
use yii\easyii\modules\catalog\api\Catalog;
use yii\easyii\modules\entity\api\Entity;
use yii\easyii\modules\gallery\api\Gallery;
use yii\easyii\modules\guestbook\api\Guestbook;
use yii\easyii\modules\page\api\Page;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;
use app\models\Feedback as FeedbackModel;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Главная страница
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'page'=>$this->loadPage('index'),
            'girls'=>Catalog::cat('devuski')->getItems(),
            'akcii'=>Carousel::items(),
            'programmy'=>Catalog::cat('programmy')->getItems(),
            'interery'=>Gallery::cat('interery')->getPhotos(),
            'salony'=>Entity::cat('salony')->getItems(),
            'guestbook' => Guestbook::items([
                'pagination' => [
                    'pageSize' => 5,
                ],
            ]),
        ]);
    }

    /**
     * Страница работы
     * @return string
     */
    public function actionRabota()
    {
        return $this->render('work', [
            'page'=>$this->loadPage('rabota'),
            'interery'=>Gallery::cat('interery')->getPhotos(),
            'salony'=>Entity::cat('salony')->getItems(),
        ]);
    }

    /**
     * Страница отзывов
     * @return string
     */
    public function actionOtzyvy()
    {
        return $this->render('//guestbook/index', [
            'page'=>$this->loadPage('otzyvy'),
            'guestbook' => Guestbook::items(),
            'salony'=>Entity::cat('salony')->getItems(),
        ]);
    }

    /**
     * @param string $slug
     * @return string
     * @throws NotFoundHttpException
     */
    protected function loadPage($slug){
        $page = Page::get($slug);
        if (!$page) {
            throw new NotFoundHttpException('Item not found.');
        }
        return $page;
    }

    /**
     * Обратный заказ с сайта
     * @return \yii\web\Response
     */
    public function actionFeedback()
    {
        $model = new FeedbackModel;

        $request = Yii::$app->request;

        if ($model->load($request->post())) {
            $returnUrl = $model->save() ? $request->post('successUrl') : $request->post('errorUrl');
            return $this->redirect($returnUrl);
        } else {
            return $this->redirect(Yii::$app->request->baseUrl);
        }
    }

    /**
     * Оставить отзыв
     * @return \yii\web\Response
     */
    public function actionGuestbook()
    {
        $model = new GuestbookForm();

        $request = Yii::$app->request;

        if ($model->load($request->post())) {
            $returnUrl = $model->save() ? $request->post('successUrl') : $request->post('errorUrl');
            return $this->redirect($returnUrl);
        } else {
            return $this->redirect(Yii::$app->request->baseUrl);
        }
    }
}