<?php

namespace app\controllers;

use app\models\GuestbookForm;
use yii\easyii\modules\carousel\api\Carousel;
use yii\easyii\modules\catalog\api\Catalog;
use yii\easyii\modules\entity\api\Entity;
use yii\easyii\modules\gallery\api\Gallery;
use yii\easyii\modules\guestbook\api\Guestbook;
use yii\easyii\modules\page\api\Page;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;
use app\models\Feedback as FeedbackModel;

class GirlsController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Список девушек
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'page'=>$this->loadPage('girls'),
            'girls'=>Catalog::cat('devuski')->getItems(),
            'salony'=>Entity::cat('salony')->getItems(),
            'guestbook' => Guestbook::items([
                'pagination' => [
                    'pageSize' => 5,
                ],
            ]),
        ]);
    }

    /**
     * Профайл девушки
     * @param string $slug
     * @return string
     */
    public function actionView($slug)
    {
        return $this->render('view', [
            'page'=>$this->loadPage('girls'),
            'girl'=>Catalog::get($slug),
            'girls'=>Catalog::cat('devuski')->getItems(),
            'salony'=>Entity::cat('salony')->getItems(),
            'guestbook' => Guestbook::items([
                'pagination' => [
                    'pageSize' => 5,
                ],
            ]),
        ]);
    }

    /**
     * @param string $slug
     * @return string
     * @throws NotFoundHttpException
     */
    protected function loadPage($slug){
        $page = Page::get($slug);
        if (!$page) {
            throw new NotFoundHttpException('Item not found.');
        }
        return $page;
    }
}