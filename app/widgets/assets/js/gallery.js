jQuery(function($) {

    var body = $("body");
    var Gallery = (function() {

        var galleryWrapper = $(".girlsGallery-wrapper");
        var scrollBox = $(".girlsGallery-scrollbox");
        var masseuseInput = $("#appointmentform-masseuse");
        var thumbnails = $("#girlsGallery-modal-collection");

        var Gallery = {
            position: 0,
            sources: null,
            init: function(gallery) {
                // Обновление данных
                this.position = 0;
                this.sources = $("#" + gallery.data("gallery") + " img");
                masseuseInput.prop("value", gallery.data("masseuse"));
                // Заголок
                var name = gallery.find(".name");
                if (name.length) {
                    $(".name", galleryWrapper).text(name.text());
                }
                // Панель предпросмотра
                thumbnails.html("");
                for (var i=0; i<this.sources.length; i++) {
                    var src = $(this.sources.get(i)).data("thumb");
                    var imageThumbnails = $(
                        "<div class='img-container'>" +
                        "<img data-index='" + i + "' class='img-thumbnail' src='" + src + "' />" +
                        "</div>"
                    );
                    thumbnails.append(imageThumbnails);
                }
            },
            show: function() {
                this.scrollHeight();
                this.change(this.position);
                galleryWrapper.fadeIn(250);
            },
            close: function() {
                galleryWrapper.fadeOut(250);
            },
            next: function() {
                this.position++;
                if (this.position >= this.sources.length) {
                    this.position = 0;
                }
                this.change(this.position);
            },
            prev: function() {
                this.position--;
                if (this.position < 0) {
                    this.position = this.sources.length - 1;
                }
                this.change(this.position);
            },
            change: function(index) {
                var item = $(this.sources.get(index));
                galleryWrapper
                    .find(".current-image")
                    .attr("src", item.attr("src"));
            },
            scrollHeight: function() {
                var maxHeight = $(window).height() - 50;
                scrollBox.css("min-height", "585px");
                scrollBox.css("max-height", maxHeight + "px");
                /*scrollBox.css("max-height", "100%");*/
            }
        };

        return Gallery;

    })();

    $(".girl-wrapper").on("click", function(e) {
        //var scrollTo = body.scrollTop();
        //body.addClass("noscroll");

        Gallery.init($(this));
        Gallery.show();
    });
    body.on("click", ".girlsGallery-right", function(e) {
        e.preventDefault();
        Gallery.next();
    });
    body.on("click", ".girlsGallery-left", function(e) {
        e.preventDefault();
        Gallery.prev();
    });

    body.on("click", ".girlsGallery-wrapper", function(e) {
        if ($(e.target).hasClass("girlsGallery-wrapper")) {
            Gallery.close();
        }
    });

    body.on("click", "#girlsGallery-modal-collection img", function(e) {
        Gallery.change($(this).data("index"));
    });

    body.on("click", ".girlsGallery-close", function(e) {
        e.preventDefault();
        Gallery.close();
    });

    $(window).resize(function() {
        // set event on resize
        clearTimeout(this.id);
        this.id = setTimeout(Gallery.scrollHeight, 100);
    });

    document.onkeydown = function(evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            Gallery.close();
        }
    };
});
