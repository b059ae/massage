<?php
namespace app\widgets;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/**
 * Форма отзыва
 * Class GuestbookForm
 * @package app\widgets
 */
class GuestbookForm extends AbstractAjaxForm
{

    public $button = 'Оставить отзыв';
    public $thankTitle = 'Спасибо за Ваш отзыв!';
    public $thankText = '';
    public $metrikaGoal = 'guestbook';
    public $layout = 'default';

    public function run()
    {
        $model = new \app\models\GuestbookForm();

        $form = ActiveForm::begin([
            'id' => $this->formId,
            'layout' => $this->layout,
            'enableClientValidation' => true,
            'action' => Url::to(['/site/guestbook']),
            'options'=>[
                'class' => 'text-center',
            ]
        ]);

        /*echo Html::hiddenInput('errorUrl', Url::current());
        echo Html::hiddenInput('successUrl', Url::current());*/

        /*echo $form->field($model, 'name')->textInput();*/

        echo $form->field($model, 'name',[
            'template' => "<p>{label}</p>\n{input}\n{error}",
        ])->textInput();

        echo $form->field($model, 'text',[
            'template' => "<p>{label}</p>\n{input}\n{error}",
        ])->textarea([
            'rows' => 5,
        ]);

        echo "<div class='jdu form-group'>" . Html::submitButton($this->button, ['class' => 'btn btn-info', 'onclick'=>"metrikaReachGoal('".$this->metrikaGoal."')"]) . "</div>";
        ActiveForm::end();

        $this->registerJs();
    }

}