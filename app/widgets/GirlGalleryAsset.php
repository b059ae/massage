<?php

namespace app\widgets;

use yii\web\AssetBundle;

class GirlGalleryAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/assets';
    public $css = [
        'css/gallery.css',
    ];
    public $js = [
        'js/gallery.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
