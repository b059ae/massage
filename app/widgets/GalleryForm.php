<?php
namespace app\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/**
 * Форма обратного звонка
 * Class GalleryForm
 * @package app\widgets
 */
class GalleryForm extends AbstractAjaxForm
{

    public $button = 'Записаться';//Получить скидку
    public $thankTitle = 'Спасибо за Вашу заявку!';
    public $thankText = 'Мы оперативно свяжемся и проконсультируем Вас.';
    public $metrikaGoal = 'callback-girl';
    public $layout = 'default';

    public function run()
    {
        $model = new \app\models\CallbackForm([
            'name'=>'Посетитель',
        ]);

        $form = ActiveForm::begin([
            'id' => $this->formId,
            'layout' => $this->layout,
            'enableClientValidation' => true,
            'action' => Url::to(['/site/feedback'])
        ]);

        /*echo Html::hiddenInput('errorUrl', Url::current());
        echo Html::hiddenInput('successUrl', Url::current());*/

        /*echo $form->field($model, 'name')->textInput();*/

        echo '<div class="row">';
        echo '<div class="col-md-6 col-lg-offset-3">';

        echo $form->field($model, 'name',[
            'template' => "{input}\n{error}",
            'inputOptions' => [
                'class' => 'form-control input-lg',
            ],
        ])->hiddenInput();

        echo $form->field($model, 'phone',[
            'template' => "{input}\n{error}",
        ])->textInput()->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7 (999) 999 99 99',
            'options' => [
                'autocomplete' => 'off',
                'id' => 'form-gallery-feedback-phone',
                'class' => 'form-control',
                'placeholder' => $model->getAttributeLabel('phone')
            ],
        ]);
        echo '</div>';
        echo '<div class="col-xs-5 col-md-12">';

        echo "<div class='jdu form-group'>" . Html::submitButton($this->button, ['class' => 'btn btn-info btn-lg', 'onclick'=>"metrikaReachGoal('".$this->metrikaGoal."')"]) . "</div>";

        echo '</div>';
        echo '</div>';
        ActiveForm::end();

        $this->registerJs();
    }

}