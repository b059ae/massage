<?php

namespace app\widgets;

use app\models\AppointmentForm;
use yii\bootstrap\Widget;

class GirlGalleryWidget extends Widget
{
    /**
     * @var AppointmentForm
     */
    public $signup;

    public function run()
    {
        if (empty($this->signup)) {
            $this->signup = new AppointmentForm();
        }
        $this->registerClientScript();
        return $this->render('gallery', [
            'signup' => $this->signup,
        ]);
    }

    public function registerClientScript()
    {
        //GirlGalleryAsset::register($this->getView());
    }
}
