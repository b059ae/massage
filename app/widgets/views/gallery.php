<?php
/**
 * @var $signup \app\models\AppointmentForm
 */
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>

<div class="girlsGallery-wrapper" style="display: none;">
    <div class="girlsGallery-modal">
        <div class="container">
            <div class="girlsGallery-scrollbox">
                <div class="girlsGallery-modal-image">
                    <span class="girlsGallery-control girlsGallery-close">
                        <a href="#">x</a>
                    </span>
                    <span class="girlsGallery-control girlsGallery-left">
                        <a href="#">&lt;</a>
                    </span>
                    <span class="girlsGallery-control girlsGallery-right">
                        <a href="#">&gt;</a>
                    </span>
                    <img class="current-image" src="" />
                </div>
                <div class="girlsGallery-modal-text">

                    <h4 class="name">Записаться на прием</h4>
                    <div id="appointment-form">
                    <?= \app\widgets\GalleryForm::widget([
                        'id'=>'form-gallery',
                    ]) ?>

                        <?php /* $form = ActiveForm::begin([
                            'id' => 'form-reg',
                            'enableClientValidation' => true,
                            'action' => Url::to(['/site/signup'])
                        ]); ?>

                            <?= $form->field($signup, 'phone', [
                                'template' => "{input}\n{error}",
                                'inputOptions' => [
                                    'class' => 'form-control input-lg',
                                ],
                            ])
                            ->textInput([
                                'placeholder' => $signup->getAttributeLabel('phone')
                            ]); ?>

                            <?= $form->field($signup, 'masseuse', [
                                'template' => "{input}",
                            ])
                            ->hiddenInput(); ?>
                            <div class="btn-send">
                                <input type="submit" id="btn-send" value="Записаться" class="btn btn-info btn-lg">
                            </div>
                        <?php ActiveForm::end();  */ ?>
                    </div>
                    <p>Перезвоним за 27 секунд</p>

                    <div id="girlsGallery-modal-collection">

                    </div>
                    <div class="clearfix"></div>
                    <p>
                        <a href="/otzyvy">Посмотреть все отзывы</a>
                    </p>
                    <?php //echo "<div class='jdu form-group'>" . Html::submitButton($this->button, ['class' => 'btn btn-info btn-lg', 'onclick'=>"metrikaReachGoal('".$this->metrikaGoal."')"]) . "</div>"; ?>
                    <a class="btn btn-info btn-lg modal-window-button" href="#form-guestbook-modal">Оставить отзыв</a>
                </div>
            </div>
        </div>
    </div>
</div>
