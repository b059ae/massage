<?php
/** @var $this \yii\web\View */
/** @var $id string */
/** @var $title string */
/** @var $text string */

?>

<div id="<?= $id; ?>" class="modal-window-wrapper" style="display: none;">
    <div class="modal-window-container">
        <div class="modal-window-header">
            <h4 class="modal-window-title"><?= $title; ?></h4>
        </div>
        <div class="overlay-window">
            <p><?= $text; ?></p>
            <p class="text-right"><a class="btn btn-info modal-window-close" href="#">Ок</a></p>
        </div>
    </div>
</div>
