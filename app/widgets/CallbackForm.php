<?php
namespace app\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/**
 * Форма обратного звонка
 * Class CallbackForm
 * @package app\widgets
 */
class CallbackForm extends AbstractAjaxForm
{

    public $button = 'Записаться';//Получить скидку
    public $thankTitle = 'Спасибо за Вашу заявку!';
    public $thankText = 'Мы оперативно свяжемся и проконсультируем Вас.';
    public $metrikaGoal = 'callback';
    public $layout = 'default';

    public function run()
    {
        $model = new \app\models\CallbackForm([
            'name'=>'Посетитель',
        ]);

        $form = ActiveForm::begin([
            'id' => $this->formId,
            'layout' => $this->layout,
            'enableClientValidation' => true,
            'action' => Url::to(['/site/feedback']),
            'options'=>[
                'class' => 'text-center',
            ]
        ]);

        /*echo Html::hiddenInput('errorUrl', Url::current());
        echo Html::hiddenInput('successUrl', Url::current());*/

        /*echo $form->field($model, 'name')->textInput();*/

        echo $form->field($model, 'name',[
            'template' => "{input}\n{error}",
        ])->hiddenInput();

        echo $form->field($model, 'phone',[
            'template' => "{input}\n{error}",
        ])->textInput()->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7 (999) 999 99 99',
            'options' => [
                'autocomplete' => 'off',
                'id' => $this->formId.'-feedback-phone',
                'class' => 'appointment-phone-input form-control',
                'placeholder' => $model->getAttributeLabel('phone')
            ],
        ]);

        echo "<div class='jdu form-group'>" . Html::submitButton($this->button, ['class' => 'btn btn-info', 'onclick'=>"metrikaReachGoal('".$this->metrikaGoal."')"]) . "</div>";
        ActiveForm::end();

        $this->registerJs();
    }

}