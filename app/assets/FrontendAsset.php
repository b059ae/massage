<?php
namespace app\assets;

class FrontendAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        //'css/css-roboto.css',
        'css/styles.css?20170823',
        'css/bone_style.css',
        'css/common.css?20170823',
        'css/flexslider.css',
        'css/colorbox.css',
        'css/css-font.css',
        'css/call.css',
    ];
    public $js = [
        //'js/jquery.tubular.1.0.js',
        //'js/jquery.js',
        //'js/jquery-migrate.min.js',
        'js/modernizr.custom.min.js',
        'js/jquery.flexslider.min.js',
        'js/jQuery.easing.min.js',
        'js/jquery.colorbox.js',
        'js/script.js',
    ];
    public $depends = [
        //'app\widgets\GirlGalleryAsset',
    ];
}
