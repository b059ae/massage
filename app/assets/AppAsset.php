<?php
namespace app\assets;

class AppAsset extends \yii\web\AssetBundle
{
    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\widgets\ActiveFormAsset',
        'yii\validators\ValidationAsset',
        //'yii\widgets\MaskedInputAsset',//Не работает со сжатием
        //'app\widgets\GirlGalleryAsset',
    ];
}
